-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2018 at 02:57 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_repositories`
--

CREATE TABLE `acl_repositories` (
  `id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `role_name` varchar(200) NOT NULL,
  `resource_name` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `role_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `all_images`
--

CREATE TABLE `all_images` (
  `image_id` int(10) UNSIGNED NOT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `image_path` text COLLATE utf8mb4_unicode_ci,
  `image_type_id` int(11) DEFAULT NULL,
  `image_display_type` enum('front','bg') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_source_type` enum('category','store') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `app_skinning`
--

CREATE TABLE `app_skinning` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `skin_title` varchar(200) NOT NULL,
  `json` text NOT NULL,
  `status` varchar(8) NOT NULL,
  `skin_image` varchar(100) NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auto_checkout`
--

CREATE TABLE `auto_checkout` (
  `id` int(10) UNSIGNED NOT NULL,
  `business_id` int(11) DEFAULT NULL,
  `cart_discount` varchar(255) DEFAULT NULL,
  `cart_sku` varchar(255) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `columns` varchar(255) DEFAULT NULL,
  `cstHospitalityCourseid` int(11) DEFAULT NULL,
  `custom_price_id` int(11) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  `discount_type` varchar(255) DEFAULT NULL,
  `discount_value` varchar(255) DEFAULT NULL,
  `discountable` varchar(255) DEFAULT NULL,
  `is_done_order` int(11) DEFAULT NULL,
  `live_order_id` int(11) DEFAULT NULL,
  `max_discount_amount` varchar(255) DEFAULT NULL,
  `max_discount_percentage` varchar(255) DEFAULT NULL,
  `modifier_ids` varchar(255) DEFAULT NULL,
  `modifiers_name` varchar(255) DEFAULT NULL,
  `prd_color` varchar(255) DEFAULT NULL,
  `prd_cost` varchar(255) DEFAULT NULL,
  `prd_description` varchar(255) DEFAULT NULL,
  `prd_id` int(11) DEFAULT NULL,
  `prd_image` varchar(255) DEFAULT NULL,
  `prd_modifier_cost` varchar(255) DEFAULT NULL,
  `prd_modifier_price` varchar(255) DEFAULT NULL,
  `prd_name` varchar(255) DEFAULT NULL,
  `prd_parent_id` int(11) DEFAULT NULL,
  `prd_parent_name` varchar(255) DEFAULT NULL,
  `prd_qty` int(11) DEFAULT NULL,
  `prd_trigger` int(11) DEFAULT NULL,
  `prd_type` varchar(255) DEFAULT NULL,
  `prd_unit_price` varchar(255) DEFAULT NULL,
  `product_array` text,
  `qty_onhand` varchar(255) DEFAULT NULL,
  `recom_order` int(11) DEFAULT NULL,
  `table_id` varchar(255) DEFAULT NULL,
  `taxable` varchar(255) DEFAULT NULL,
  `total_items_discount` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `variants` varchar(255) DEFAULT NULL,
  `vat_amount` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auto_order_placement`
--

CREATE TABLE `auto_order_placement` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_detail` text,
  `venue_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `operation_performed` enum('default','checkout','cancel','pos','auto_payment_done','auto_payment_failed','is_deleted') DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `beacon_configurations`
--

CREATE TABLE `beacon_configurations` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `beacon_name` varchar(255) NOT NULL,
  `beacon_type` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `major` varchar(11) NOT NULL,
  `minor` varchar(11) NOT NULL,
  `x_coordinate` varchar(255) NOT NULL,
  `y_coordinate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `type` int(5) DEFAULT NULL COMMENT '1: set_forget, 2: proximity, 3: standard',
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `target_segments` varchar(200) DEFAULT NULL,
  `target_type` varchar(155) DEFAULT NULL,
  `target_value` text,
  `action_type` varchar(100) DEFAULT NULL,
  `action_value` text,
  `send_cost` varchar(255) DEFAULT NULL,
  `reward_cost` varchar(255) DEFAULT NULL,
  `schedule_type` varchar(150) DEFAULT NULL,
  `schedule_value` text,
  `status` varchar(50) NOT NULL DEFAULT 'Draft' COMMENT '&#039;Active&#039;,&#039;Scheduled&#039;,&#039;Draft&#039;,&#039;Paused&#039;,&#039;Completed&#039;',
  `sent_date` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_logs`
--

CREATE TABLE `campaign_logs` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_tags`
--

CREATE TABLE `campaign_tags` (
  `tag_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `pos_category_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_id` bigint(20) DEFAULT NULL,
  `category_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_description` text COLLATE utf8mb4_unicode_ci,
  `category_parent_id` int(11) DEFAULT '0',
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_priority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `pos_code`, `pos_category_id`, `store_id`, `category_title`, `category_description`, `category_parent_id`, `author`, `created_at`, `updated_at`, `category_type`, `category_color`, `display_priority`) VALUES
(225, 1, '5455', 86, 'Accessories', '', 0, NULL, '2018-10-18 07:56:03', NULL, '1', '66b447', '5'),
(226, 1, '5433', 86, 'Jumpers', '', 0, NULL, '2018-10-18 07:56:03', NULL, '1', '996644', '4'),
(227, 1, '5432', 86, 'Jeans', '', 0, NULL, '2018-10-18 07:56:03', NULL, '1', '216bd6', '3'),
(228, 1, '5431', 86, 'T-shirts', '', 0, NULL, '2018-10-18 07:56:03', NULL, '1', '', '2');

-- --------------------------------------------------------

--
-- Table structure for table `cms_levels`
--

CREATE TABLE `cms_levels` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `setting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credit_cards`
--

CREATE TABLE `credit_cards` (
  `card_id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_cvv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_exp_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line1` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line2` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emails_templates`
--

CREATE TABLE `emails_templates` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `email_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `cost` varchar(100) DEFAULT NULL,
  `stall` varchar(100) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `store_logo` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_time` varchar(100) DEFAULT NULL,
  `end_time` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favorite_products`
--

CREATE TABLE `favorite_products` (
  `favorite_product_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `favourite_news`
--

CREATE TABLE `favourite_news` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file_repositories`
--

CREATE TABLE `file_repositories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `venue_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `path` varchar(250) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file_uploads`
--

CREATE TABLE `file_uploads` (
  `id` int(11) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `template_desc` text NOT NULL,
  `venue_id` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `path` longtext,
  `content` longblob NOT NULL,
  `created_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fuel_stations`
--

CREATE TABLE `fuel_stations` (
  `fuel_station_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `fuel_station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuel_station_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuel_start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuel_end_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `external_static_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuel_station_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuel_station_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuel_station_lat_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pre_pay` tinyint(1) NOT NULL DEFAULT '0',
  `post_pay` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_description` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE `group_members` (
  `group_member_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gyms`
--

CREATE TABLE `gyms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gym_excluded_business`
--

CREATE TABLE `gym_excluded_business` (
  `gym_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image_category`
--

CREATE TABLE `image_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  `application_id` bigint(20) DEFAULT NULL,
  `platform` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '18',
  `secret` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `knox_users`
--

CREATE TABLE `knox_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `levels_venues`
--

CREATE TABLE `levels_venues` (
  `id` int(11) NOT NULL,
  `level_id` varchar(200) NOT NULL,
  `venue_id` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `level_configurations`
--

CREATE TABLE `level_configurations` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `venue_level` int(11) NOT NULL,
  `floor_number` varchar(255) NOT NULL,
  `floor_name` varchar(255) NOT NULL,
  `floor_plan` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loyalty_configurations`
--

CREATE TABLE `loyalty_configurations` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `rate_grade_1` varchar(255) DEFAULT NULL,
  `rate_grade_2` varchar(255) DEFAULT NULL,
  `rate_grade_3` varchar(255) DEFAULT NULL,
  `rate_grade_4` varchar(255) DEFAULT NULL,
  `rate_grade_5` varchar(255) DEFAULT NULL,
  `rate_grade_6` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_earn_velues`
--

CREATE TABLE `lt_earn_velues` (
  `lt_earn_velue_id` int(11) NOT NULL,
  `lt_point_id` int(11) NOT NULL,
  `price_value` varchar(255) NOT NULL,
  `points_velue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_expiry_logs`
--

CREATE TABLE `lt_expiry_logs` (
  `lt_expiry_log_id` int(11) NOT NULL,
  `lt_transation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lt_point_id` int(11) NOT NULL,
  `point_exp_id` int(11) NOT NULL,
  `total_points` int(11) NOT NULL,
  `expir_percent` int(11) NOT NULL,
  `wasted_points` int(11) NOT NULL,
  `remaining_points` int(11) NOT NULL,
  `redeemed_points` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_exp_points`
--

CREATE TABLE `lt_exp_points` (
  `id` int(11) NOT NULL,
  `lt_point_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `time_span` varchar(50) NOT NULL COMMENT 'Days,Weeks,Months,Years',
  `no_of_days` int(11) NOT NULL,
  `exp_percent` varchar(100) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_points`
--

CREATE TABLE `lt_points` (
  `lt_point_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `lt_point_title` varchar(255) NOT NULL,
  `lt_point_description` text NOT NULL,
  `lt_point_type` varchar(255) NOT NULL,
  `redeemable` int(11) NOT NULL DEFAULT '0',
  `transfarable` int(11) NOT NULL DEFAULT '0',
  `expairable` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL,
  `time_span` varchar(255) NOT NULL,
  `assigned_message` text,
  `redeemable_message` text,
  `expairable_message` text,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_redeem_values`
--

CREATE TABLE `lt_redeem_values` (
  `lt_redeem_value_id` int(11) NOT NULL,
  `lt_point_id` int(11) NOT NULL,
  `red_price_velue` varchar(255) NOT NULL,
  `red_point_velue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_role`
--

CREATE TABLE `lt_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `role_date_added` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lt_role_assign`
--

CREATE TABLE `lt_role_assign` (
  `role_asg_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `store_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lt_rules`
--

CREATE TABLE `lt_rules` (
  `rule_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `rule_venue_id` int(11) NOT NULL,
  `store_id` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `rule_for` int(11) NOT NULL COMMENT '1 for category 2 for product 3 for store 4 for action',
  `rule_title` varchar(255) NOT NULL,
  `rule_description` text NOT NULL,
  `rule_tier_id` varchar(255) NOT NULL,
  `rule_product_id` varchar(255) NOT NULL,
  `rule_product_qty` int(11) NOT NULL,
  `rule_point_id` int(11) NOT NULL,
  `rule_point_qty` int(11) NOT NULL,
  `rule_start_date` varchar(255) NOT NULL,
  `rule_end_date` varchar(255) NOT NULL,
  `rule_type` int(11) NOT NULL COMMENT '1 for unlimited and 2 for limited',
  `rule_max_limit` int(11) NOT NULL,
  `transaction_type` int(11) NOT NULL COMMENT '1 for any amount 2 for greater then specific amount',
  `transaction_amount` decimal(10,0) NOT NULL,
  `rule_action` varchar(255) NOT NULL,
  `rule_status` int(11) NOT NULL DEFAULT '1',
  `rule_preference` int(11) NOT NULL,
  `rule_is_exclusive` int(11) NOT NULL,
  `rule_date_added` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lt_rule_options`
--

CREATE TABLE `lt_rule_options` (
  `rule_option_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `opt_type` int(11) NOT NULL,
  `opt_tier` int(11) NOT NULL,
  `opt_qty_con` varchar(255) NOT NULL,
  `opt_qty` int(11) NOT NULL,
  `opt_tran_amt_con` varchar(255) NOT NULL,
  `opt_tran_amt` int(11) NOT NULL,
  `opt_tran_tot_con` varchar(255) NOT NULL,
  `opt_tran_tot` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lt_rule_rewards`
--

CREATE TABLE `lt_rule_rewards` (
  `rule_reward_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `reward_type` int(11) NOT NULL COMMENT '1 for points 2 for tiers',
  `rule_point_id` int(11) NOT NULL,
  `rule_point_qty` int(11) NOT NULL,
  `rule_tier_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lt_store_earn_velues`
--

CREATE TABLE `lt_store_earn_velues` (
  `lt_st_earn_velue_id` int(11) NOT NULL,
  `lt_point_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `price_value` varchar(255) NOT NULL,
  `points_velue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_store_managers`
--

CREATE TABLE `lt_store_managers` (
  `lt_store_manager_id` int(11) NOT NULL,
  `venue_man_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_tiers`
--

CREATE TABLE `lt_tiers` (
  `lt_tier_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lt_tier_title` varchar(255) NOT NULL,
  `lt_tier_description` text NOT NULL,
  `is_demoteable` int(11) NOT NULL DEFAULT '0',
  `predecessor` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_tier_rules`
--

CREATE TABLE `lt_tier_rules` (
  `lt_tier_rule_id` int(11) NOT NULL,
  `lt_tier_id` int(11) NOT NULL,
  `lt_point_id` int(11) NOT NULL,
  `min_value_points` int(11) NOT NULL,
  `lt_tier_relation` varchar(11) NOT NULL DEFAULT '0',
  `tier_status` int(11) NOT NULL DEFAULT '1',
  `fuel_discount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_transations`
--

CREATE TABLE `lt_transations` (
  `lt_transation_id` int(11) NOT NULL,
  `soldi_id` int(11) NOT NULL,
  `soldi_transaction_id` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `lt_rule_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_amount` decimal(10,2) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `point_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_price` decimal(10,0) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `rule_for` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL,
  `expired_points` double NOT NULL,
  `redeemed_points` double NOT NULL,
  `wasted_points` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lt_user_tier`
--

CREATE TABLE `lt_user_tier` (
  `lt_user_tier_id` int(11) NOT NULL,
  `tier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `user_points` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `news_category_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `level_id` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) DEFAULT '0',
  `news_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `news_tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_image_gif` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `news_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `news_is_featured` int(11) NOT NULL,
  `is_public` int(11) NOT NULL DEFAULT '0',
  `news_web_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

CREATE TABLE `news_categories` (
  `news_category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `news_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `news_category_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `offer_id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `extended_description` text,
  `image` varchar(200) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `barcode_value` varchar(100) DEFAULT NULL,
  `is_redeemed` int(1) NOT NULL DEFAULT '0',
  `expire_date` date DEFAULT NULL,
  `voucher_id` varchar(100) DEFAULT NULL,
  `price` varchar(100) NOT NULL,
  `is_special` int(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) DEFAULT NULL,
  `third_party` int(1) NOT NULL DEFAULT '0',
  `third_party_url` varchar(200) DEFAULT NULL,
  `redeem_type` int(1) DEFAULT '0',
  `redeem_limit` varchar(100) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offer_categories`
--

CREATE TABLE `offer_categories` (
  `offer_category_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_set_id` bigint(20) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_price` decimal(10,2) DEFAULT NULL,
  `option_cost` decimal(10,2) DEFAULT '0.00',
  `option_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `option_sets`
--

CREATE TABLE `option_sets` (
  `option_set_id` bigint(20) UNSIGNED NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `pos_option_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_set_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_selection` int(11) DEFAULT '0',
  `max_selection` int(11) DEFAULT '0',
  `option_set_status` tinyint(4) DEFAULT NULL,
  `choose_opt_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `payment_method_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status` int(11) DEFAULT '0',
  `order_note` text COLLATE utf8mb4_unicode_ci,
  `order_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_amount` decimal(10,2) DEFAULT '0.00',
  `order_pin_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_acknowledge` int(11) DEFAULT NULL,
  `ord_table` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waiter_id` int(11) DEFAULT NULL,
  `device_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_points` int(11) DEFAULT NULL,
  `business_id` bigint(20) DEFAULT NULL,
  `currency` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surcharges` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_label` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_amount` decimal(10,2) DEFAULT NULL,
  `items_discount` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `is_soldi` tinyint(1) DEFAULT NULL,
  `change_due` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tip_amount` decimal(10,2) DEFAULT NULL,
  `invoice_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `points_amount` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `venue_id` int(11) DEFAULT NULL,
  `pos_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `order_detail_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `unit_price` decimal(10,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `pm_id` int(10) UNSIGNED NOT NULL,
  `pm_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pm_ledger_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `point_type`
--

CREATE TABLE `point_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `pt_id` int(11) DEFAULT NULL,
  `tracking_type_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `period_type` int(11) DEFAULT NULL,
  `aging_periods` int(11) DEFAULT NULL,
  `first_period` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `one_monetary_unit` double DEFAULT NULL,
  `system` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pos`
--

CREATE TABLE `pos` (
  `id` int(10) UNSIGNED NOT NULL,
  `pos_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pos_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pos_mapper`
--

CREATE TABLE `pos_mapper` (
  `id` int(10) UNSIGNED NOT NULL,
  `pos_id` int(11) NOT NULL,
  `service_code` int(11) NOT NULL,
  `service_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fields` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pos_mapper`
--

INSERT INTO `pos_mapper` (`id`, `pos_id`, `service_code`, `service_name`, `table_name`, `fields`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Soldi stores', 'store', '{\"pos_store_id\":\"business_id\",\"store_name\":\"business_name\",\"store_phone\":\"business_mobile\",\"store_email\":\"business_email\",\"store_address\":\"business_location\",\"store_owner\":\"business_owner\",\"store_account_type\":\"business_account_type\",\"store_detail\":\"business_detail\",\"store_detail_info\":\"business_detail_info\",\"store_api_key\":\"business_api_key\",\"store_api_secret\":\"business_secret\",\"pos_user_id\":\"user_id\"}', 1, '2017-06-13 19:00:00', '2017-06-13 19:00:00'),
(2, 1, 2, 'Soldi categories', 'category', '{\"pos_category_id\":\"cate_id\",\"category_title\":\"cate_name\",\"category_description\":\"cate_detail\",\"category_type\":\"cate_type\",\"category_color\":\"cate_color\",\"display_priority\":\"displaypriority\"}', 1, '2017-06-13 19:00:00', '2017-06-13 19:00:00'),
(3, 1, 3, 'Soldi products', 'product', '{\"pos_product_id\":\"prd_id\",\"product_name\":\"prd_name\",\"product_description\":\"prd_description\",\"product_price\":\"prd_price\",\"product_stock\":\"qty_onhand\",\"display_priority\":\"displaypriority\",\"product_sku\":\"prd_sku\",\"parent_id\":\"parent_id\",\"product_columns\":\"columns\",\"product_variants\":\"variants\",\"product_trigger\":\"prd_trigger\",\"recom_order\":\"recom_order\",\"discountable\":\"discountable\",\"max_discount_amount\":\"max_discount_amount\",\"max_discount_percentage\":\"max_discount_percentage\",\"taxable\":\"taxable\",\"product_color\":\"prd_color\",\"product_code\":\"upc_code\",\"product_cost\":\"prd_cost\",\"board_cate_id\":\"board_cate_id\",\"short_description\":\"short_description\"}', 1, '2017-06-13 19:00:00', '2017-06-13 19:00:00'),
(4, 1, 4, 'Soldi Variants', 'variant', '{\"pos_variant_id\":\"prd_id\",\"variant_name\":\"prd_name\",\"variant_description\":\"prd_description\",\"variant_price\":\"prd_price\",\"qty_onhand\":\"qty_onhand\",\"display_priority\":\"displaypriority\",\"variant_sku\":\"prd_sku\",\"parent_id\":\"parent_id\",\"variant_columns\":\"columns\",\"variant_variants\":\"variants\",\"variant_trigger\":\"prd_trigger\",\"recom_order\":\"recom_order\",\"discountable\":\"discountable\",\"max_discount_amount\":\"max_discount_amount\",\"max_discount_percentage\":\"max_discount_percentage\",\"taxable\":\"taxable\",\"variant_color\":\"prd_color\",\"variant_code\":\"upc_code\",\"variant_cost\":\"prd_cost\",\"board_cate_id\":\"board_cate_id\",\"variant_img\":\"prd_image\"}', 1, '2017-06-19 19:00:00', '2017-06-19 19:00:00'),
(5, 1, 5, 'Soldi option sets', 'option_sets', '{\"pos_option_id\":\"modi_id\",\"option_set_name\":\"modi_name\",\"max_selection\":\"modi_max_selected\",\"option_set_status\":\"modi_status\",\"choose_opt_label\":\"choose_opt_label\"}', 1, '2017-06-19 19:00:00', '2017-06-19 19:00:00'),
(6, 1, 6, 'Soldi Options', 'options', '{\"option_set_id\":\"modi_id\",\"option_name\":\"modi_opt_name\",\"option_price\":\"modi_opt_sale\",\"option_cost\":\"modi_opt_cost\",\"option_sku\":\"modi_opt_sku\"}', 1, '2017-06-19 19:00:00', '2017-06-19 19:00:00'),
(7, 2, 101, 'Get Starrtec Locations', 'store', '{\"pos_store_id\":\"Id\",\"store_name\":\"Name\",\"store_phone\":\"Phone\",\"store_email\":\"Email\",\"store_website\":\"WebSite\",\"store_address\":\"Address\"}', 1, '2017-06-19 11:15:14', '2017-06-19 11:15:23'),
(8, 2, 102, 'Get Starrtec Category', 'category', '{\"pos_category_id\":\"Id\",\"category_title\":\"Name\",\"category_description\":\"Description\"}', 1, '2017-06-19 11:15:17', '2017-06-19 11:15:25'),
(9, 2, 103, 'Get Starrtec Product', 'product', '{\"pos_product_id\":\"Id\",\"product_name\":\"Name\",\"product_description\":\"Description\",\"product_price\":\"Price\"}', 1, '2017-06-19 11:15:20', '2017-06-19 11:15:27'),
(10, 2, 104, 'Get Starrtec Options', 'option_sets', '{\"pos_option_id\":\"Id\",\"option_set_name\":\"Name\",\"min_selection\":\"Minimum\",\"max_selection\":\"Maximum\"}', 1, '2017-06-19 12:17:20', '2017-06-19 12:17:22'),
(11, 2, 105, 'Get Starrtec Variant', 'Variant', '{\"pos_variant_id\":\"Id\",\"variant_name\":\"Name\",\"variant_description\":\"Description\",\"variant_price\":\"Price\"}', 1, '2017-06-19 12:50:31', '2017-06-19 12:50:28'),
(12, 1, 7, 'Save soldi store setting data', 'store_settings', '{\"licence_fee\":\"licence_fee\",\"card_price\":\"card_price\",\"language\":\"language\",\"currency\":\"currency\",\"date_format\":\"date_format\",\"number_format\":\"number_format\",\"tax_type\":\"tax_type\",\"payment_type\":\"payment_type\",\"tip_enable\":\"tip_enable\",\"tax_value\":\"tax_value\",\"country_id\":\"country_id\",\"currency_multiple\":\"currency_multiple\",\"ticket_time\":\"ticket_time\",\"near_ticket_time\":\"near_ticket_time\",\"surcharge_value\":\"surcharge_value\",\"surcharge_type\":\"surcharge_type\",\"minimum_spend\":\"minimum_spend\",\"is_ewallet\":\"is_ewallet\"}', 1, '2017-07-10 19:00:00', '2017-07-10 19:00:00'),
(13, 3, 202, 'Save Swift Category Data', 'category', '{\"pos_category_id\":\"Id\",\"category_title\":\"Name\"}', 1, '2017-08-24 09:54:10', '2017-08-24 09:54:12'),
(14, 3, 203, 'Save Swift Country Data', 'swift_country', '{\"pos_country_id\":\"Id\",\"name\":\"Name\",\"code\":\"IsoCountryCode\"}', 1, '2017-08-24 11:15:41', '2017-08-24 11:15:43'),
(15, 3, 204, 'Save Swift Family Data', 'swift_family', '{\"pos_family_id\":\"Id\",\"name\":\"Name\"}', 1, '2017-08-24 11:35:29', '2017-08-24 11:35:31'),
(16, 3, 205, 'Save Swift Group Data', 'swift_group', '{\"pos_group_id\":\"Id\",\"name\":\"Name\"}', 1, '2017-08-25 04:48:33', '2017-08-25 04:48:36'),
(17, 3, 206, 'Save Swift Media Data', 'swift_medai', '{\"pos_media_id\":\"Id\",\"name\":\"Name\"}', 1, '2017-08-25 05:57:17', '2017-08-25 05:57:19'),
(18, 3, 207, 'Save Swift Product Data', 'product', '{\"pos_product_id\":\"Id\",\"product_name\":\"Standard\",\"product_code\":\"InventoryCode\",\"product_sku\":\"ProductGuid\",\"product_description\":\"Notes\",\"short_description\":\"Short\",\"product_price\":\"Price\"}', 1, '2017-08-25 07:36:44', '2017-08-25 07:36:47');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `pos_product_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `product_price` double(10,2) DEFAULT '0.00',
  `product_stock` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `board_cate_id` int(11) DEFAULT '0',
  `product_cost` decimal(10,2) DEFAULT '0.00',
  `taxable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_discount_percentage` decimal(10,2) DEFAULT '0.00',
  `max_discount_amount` decimal(10,2) DEFAULT '0.00',
  `discountable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recom_order` int(11) DEFAULT '0',
  `product_trigger` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variants` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_columns` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_priority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `point_type_id` int(11) DEFAULT NULL,
  `point_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `point_value` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `pos_code`, `pos_product_id`, `store_id`, `category_id`, `parent_id`, `product_name`, `product_description`, `short_description`, `product_price`, `product_stock`, `product_code`, `board_cate_id`, `product_cost`, `taxable`, `max_discount_percentage`, `max_discount_amount`, `discountable`, `recom_order`, `product_trigger`, `product_sku`, `product_color`, `product_variants`, `product_columns`, `display_priority`, `point_type_id`, `point_type`, `point_value`, `created_at`, `updated_at`) VALUES
(19, 1, '14295', 86, 225, 0, 'Blue baseball cap', '<p>Blue baseball cap</p>', '', 25.00, '9992.00', '', 0, '0.00', 'yes', '100.00', '25.00', '', 0, '20', 'ldsbe', 'ddbcbc', '', '', '1', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(20, 1, '14296', 86, 225, 0, 'Black and white woollen scarf', '<p>Black and white woollen scarf</p>', '', 50.00, '9939.00', '', 0, '0.00', 'yes', '100.00', '50.00', '', 0, '20', '3lz1vq', '', '', '', '2', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(21, 1, '14207', 86, 226, 0, 'Grey wool jumper', '<p>Grey wool jumper</p>', '', 89.00, '29997.00', '', 0, '0.00', 'yes', '100.00', '89.00', '', 0, '20', 'sd9or', '', '', '', '1', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(22, 1, '14211', 86, 226, 0, 'Black V Neck Cotton Jumper', '<p>Black V Neck Cotton Jumper</p>', '', 79.00, '39996.00', '', 0, '0.00', 'yes', '100.00', '79.00', '', 0, '20', 'hn9cd', '', '', '', '2', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(23, 1, '14290', 86, 226, 0, 'Red marl crew neck jumper', '<p>Red marl crew neck jumper</p>', '', 110.00, '39996.00', '', 0, '0.00', 'yes', '100.00', '110.00', '', 0, '20', 'q4nuzc9', '', '', '', '3', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(24, 1, '14206', 86, 227, 0, 'Stretch skinny jeans', '<p>stonewash stretch skinny jeans</p>', '', 99.00, '9999.00', '', 0, '0.00', 'yes', '100.00', '99.00', '', 0, '20', 'ln06v', '', '', '', '1', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(25, 1, '14217', 86, 227, 0, 'Designer Jeans ', '<p>Designer jeans</p>', '', 155.00, '9998.00', '', 0, '0.00', 'yes', '100.00', '155.00', '', 0, '20', 'zwnazs', '', '', '', '2', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(26, 1, '14203', 86, 228, 0, 'Red V-Neck T-Shirt', '<p>Red V-Neck T-Shirt</p>', '', 20.00, '109998.00', '', 0, '0.00', 'yes', '100.00', '20.00', '', 0, '20', 'fc1sko', 'dc143c', '', '', '1', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL),
(27, 1, '14216', 86, 228, 0, 'Blue Round Neck T-Shirt', '<p>Blue Round Neck T-Shirt</p>', '', 25.00, '9997.00', '', 0, '0.00', 'yes', '100.00', '25.00', '', 0, '20', '2br6l9', '', '', '', '2', NULL, NULL, NULL, '2018-10-18 07:56:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `prod_cat_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_set`
--

CREATE TABLE `product_option_set` (
  `product_option_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `option_set_id` bigint(20) DEFAULT NULL,
  `pos_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_promotion`
--

CREATE TABLE `product_promotion` (
  `pp_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `promotion_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variant`
--

CREATE TABLE `product_variant` (
  `pv_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `variant_id` bigint(20) DEFAULT NULL,
  `pos_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variant`
--

INSERT INTO `product_variant` (`pv_id`, `product_id`, `variant_id`, `pos_code`, `created_at`, `updated_at`) VALUES
(1, 21, 18, NULL, '2018-10-18 07:56:04', NULL),
(2, 21, 19, NULL, '2018-10-18 07:56:04', NULL),
(3, 21, 20, NULL, '2018-10-18 07:56:04', NULL),
(4, 22, 22, NULL, '2018-10-18 07:56:04', NULL),
(5, 22, 23, NULL, '2018-10-18 07:56:04', NULL),
(6, 22, 24, NULL, '2018-10-18 07:56:04', NULL),
(7, 22, 25, NULL, '2018-10-18 07:56:04', NULL),
(8, 23, 27, NULL, '2018-10-18 07:56:04', NULL),
(9, 23, 28, NULL, '2018-10-18 07:56:04', NULL),
(10, 23, 29, NULL, '2018-10-18 07:56:04', NULL),
(11, 23, 30, NULL, '2018-10-18 07:56:04', NULL),
(12, 26, 32, NULL, '2018-10-18 07:56:04', NULL),
(13, 26, 33, NULL, '2018-10-18 07:56:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `promotion_id` bigint(20) UNSIGNED NOT NULL,
  `promotion_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_amount` decimal(10,2) DEFAULT '0.00',
  `promotion_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `punch_cards`
--

CREATE TABLE `punch_cards` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `card_color` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `business_id` int(11) DEFAULT NULL,
  `no_of_use` varchar(255) DEFAULT NULL,
  `no_of_voucher` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `rule_on` enum('product','category') DEFAULT 'product',
  `voucher_amount` varchar(219) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `redemption_type` varchar(100) DEFAULT NULL,
  `frequency` varchar(50) DEFAULT NULL,
  `condition` varchar(10) DEFAULT NULL,
  `transaction_threshold` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `rate_value` decimal(10,1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `prep_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cook_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serving` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_category`
--

CREATE TABLE `recipe_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_ingredients`
--

CREATE TABLE `recipe_ingredients` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_offers`
--

CREATE TABLE `recipe_offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_preparation`
--

CREATE TABLE `recipe_preparation` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_reviews`
--

CREATE TABLE `recipe_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_tags`
--

CREATE TABLE `recipe_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `repositories`
--

CREATE TABLE `repositories` (
  `repository_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `repository_title` varchar(200) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `size` varchar(255) DEFAULT NULL,
  `owner` varchar(200) NOT NULL,
  `is_public` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles_acls`
--

CREATE TABLE `roles_acls` (
  `id` int(11) NOT NULL,
  `news_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `role_name` varchar(200) NOT NULL,
  `resource_name` varchar(200) DEFAULT NULL,
  `owner_name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `role_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_assigns`
--

CREATE TABLE `role_assigns` (
  `role_assign_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_level_users`
--

CREATE TABLE `role_level_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `segment`
--

CREATE TABLE `segment` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT 'Venue' COMMENT '1: Venue, 2: Global, 3: Segment Template',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `query` text,
  `query_parameters` text,
  `excluded_user` text,
  `included_user` text,
  `persona` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0: for not part of campaign 1: for part of any campaign',
  `is_hidden` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `customar_id` int(11) NOT NULL,
  `soldi_api_key` text NOT NULL,
  `soldi_api_secret` text NOT NULL,
  `amplify_api_key` text NOT NULL,
  `beacons_api_key` text NOT NULL,
  `settings_logo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `settings_email` varchar(255) NOT NULL,
  `pagination_limit` int(11) NOT NULL,
  `app_status` int(11) NOT NULL DEFAULT '1',
  `app_mode` int(11) NOT NULL DEFAULT '1',
  `app_name` varchar(100) NOT NULL,
  `app_env` tinyint(4) NOT NULL DEFAULT '0' COMMENT '	For App environment 0=Dev , 1 = Stag , 2 = Prod'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_transations`
--

CREATE TABLE `sp_transations` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ord_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `ord_date` date NOT NULL,
  `ord_amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storages`
--

CREATE TABLE `storages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `size` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `store_id` bigint(20) DEFAULT NULL,
  `pos_store_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `venue_id` int(11) DEFAULT '0',
  `store_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `store_owner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_account_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_detail` text COLLATE utf8mb4_unicode_ci,
  `store_detail_info` text COLLATE utf8mb4_unicode_ci,
  `store_api_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_api_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pos_user_id` bigint(20) DEFAULT '0',
  `soldi_api_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `soldi_secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `store_id`, `pos_store_id`, `pos_code`, `venue_id`, `store_name`, `store_phone`, `store_email`, `store_website`, `store_contact`, `store_address`, `created_at`, `updated_at`, `store_owner`, `store_account_type`, `store_detail`, `store_detail_info`, `store_api_key`, `store_api_secret`, `pos_user_id`, `soldi_api_key`, `soldi_secret`) VALUES
(87, 86, '848', 1, 0, 'Jane Apparel', '03030303033', 'fashion1@isptplutus.com', NULL, NULL, '-33.754243,151.285484', '2018-10-18 07:56:02', NULL, 'Lois Jane', 'retailer', 'Mens &amp; Womens fashion', '', 'MjcwOWM4YmFjNTA0NjAxNjVlZTBhOD', 'MTI2NjZkNjgwMjNjYTJkYTkxOTQ3OT', 1023, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_images`
--

CREATE TABLE `store_images` (
  `id` int(11) NOT NULL,
  `store_image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_information`
--

CREATE TABLE `store_information` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `coords1` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `stall_no` varchar(255) NOT NULL,
  `store_map` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store_products`
--

CREATE TABLE `store_products` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store_settings`
--

CREATE TABLE `store_settings` (
  `store_setting_id` int(10) UNSIGNED NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `licence_fee` decimal(10,2) DEFAULT NULL,
  `card_price` decimal(10,2) DEFAULT NULL,
  `language` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_format` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_format` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_type` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tip_enable` tinyint(4) DEFAULT NULL,
  `tax_value` decimal(10,2) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `currency_multiple` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_time` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `near_ticket_time` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surcharge_value` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surcharge_type` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_spend` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_ewallet` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `swift_country`
--

CREATE TABLE `swift_country` (
  `id` int(11) NOT NULL,
  `pos_country_id` int(11) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `swift_family`
--

CREATE TABLE `swift_family` (
  `id` int(11) NOT NULL,
  `pos_family_id` int(11) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `swift_group`
--

CREATE TABLE `swift_group` (
  `id` int(11) NOT NULL,
  `pos_group_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `swift_media`
--

CREATE TABLE `swift_media` (
  `id` int(11) NOT NULL,
  `pos_media_id` int(11) DEFAULT NULL,
  `pos_code` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pump_id` int(11) NOT NULL,
  `card_number` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `refund_price` varchar(100) NOT NULL DEFAULT '0',
  `cents_per_litre` decimal(10,2) NOT NULL COMMENT 'This is per liter discount',
  `quantity` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL DEFAULT '0',
  `grade_number` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `eway_transaction_id` varchar(100) NOT NULL,
  `loyalty_transaction_id` int(11) NOT NULL,
  `tran_log_id` int(11) NOT NULL,
  `first_visit_discount_status` int(11) NOT NULL,
  `first_visit_discount_amount` int(11) NOT NULL,
  `user_rating` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `amp_user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `soldi_id` int(11) DEFAULT NULL,
  `user_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_family_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_date_of_birth` date DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_is_active` int(11) DEFAULT NULL,
  `user_indentity_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qr_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_loyalty_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` int(11) DEFAULT '0',
  `activation_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `swift_pos_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('web','app') COLLATE utf8_unicode_ci DEFAULT 'app',
  `knox_user_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debug_mod` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_token` text COLLATE utf8_unicode_ci,
  `device_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_loggedin` tinyint(1) DEFAULT NULL,
  `subscribed_venues` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_cards`
--

CREATE TABLE `user_cards` (
  `card_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_no` text CHARACTER SET latin1 NOT NULL,
  `last_digit` varchar(255) CHARACTER SET latin1 NOT NULL,
  `card_name` varchar(255) NOT NULL,
  `expiry_month` varchar(20) NOT NULL,
  `expiry_year` int(11) NOT NULL,
  `cvv` text CHARACTER SET latin1 NOT NULL,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `card_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_test_card` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_redeemed_offers`
--

CREATE TABLE `user_redeemed_offers` (
  `user_redeemed_offer_id` int(11) NOT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `unique_id` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `store_name` varchar(200) DEFAULT NULL,
  `email_sent` int(1) DEFAULT '0',
  `is_redeemed` int(1) DEFAULT '0',
  `no_of_clicks` varchar(100) DEFAULT '0',
  `no_time_voucher_redeem` varchar(100) DEFAULT '0',
  `remaining_redemptions` varchar(100) DEFAULT NULL,
  `remaining_redemptions_user` varchar(100) DEFAULT NULL,
  `third_party` int(1) DEFAULT '0',
  `barcode` varchar(200) DEFAULT NULL,
  `barcode_value` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_vouchers`
--

CREATE TABLE `user_vouchers` (
  `id` int(11) NOT NULL,
  `voucher_id` varchar(255) NOT NULL,
  `voucher_type` varchar(255) NOT NULL,
  `campaign_id` varchar(255) NOT NULL,
  `patron_id` varchar(255) NOT NULL,
  `campaign_batch_number` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `barcode_image` varchar(100) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `redemption_date` datetime NOT NULL,
  `pos` varchar(100) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `variant`
--

CREATE TABLE `variant` (
  `variant_id` bigint(20) UNSIGNED NOT NULL,
  `pos_variant_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pos_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `variant_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_description` text COLLATE utf8mb4_unicode_ci,
  `variant_price` double(10,2) DEFAULT '0.00',
  `display_priority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_trigger` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recom_order` int(11) DEFAULT '0',
  `discountable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_discount_amount` decimal(10,2) DEFAULT '0.00',
  `max_discount_percentage` decimal(10,2) DEFAULT '0.00',
  `taxable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_cost` decimal(10,2) DEFAULT '0.00',
  `board_cate_id` int(11) DEFAULT '0',
  `variant_columns` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_variants` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_onhand` int(11) DEFAULT '0',
  `variant_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variant`
--

INSERT INTO `variant` (`variant_id`, `pos_variant_id`, `pos_code`, `parent_id`, `variant_name`, `variant_description`, `variant_price`, `display_priority`, `variant_sku`, `variant_trigger`, `recom_order`, `discountable`, `max_discount_amount`, `max_discount_percentage`, `taxable`, `variant_cost`, `board_cate_id`, `variant_columns`, `variant_variants`, `variant_color`, `qty_onhand`, `variant_img`, `variant_code`, `created_at`, `updated_at`) VALUES
(18, '14208', NULL, 14207, 'Small', '<p>Grey wool jumper</p>', 89.00, '0', 'sd9or', '20', 0, '', '89.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9999, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233696blob.png', '', '2018-10-18 07:56:04', NULL),
(19, '14209', NULL, 14207, 'Medium', '<p>Grey wool jumper</p>', 89.00, '0', 'lo4s3g', '20', 0, '', '89.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9998, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233696blob.png', '', '2018-10-18 07:56:04', NULL),
(20, '14210', NULL, 14207, 'Large', '<p>Grey wool jumper</p>', 89.00, '0', 'e52o0i', '20', 0, '', '89.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9990, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233696blob.png', '', '2018-10-18 07:56:04', NULL),
(22, '14212', NULL, 14211, 'X-Small', '<p>Black V Neck Cotton Jumper</p>', 79.00, '0', 'hn9cd', '20', 0, '', '79.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9999, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233794blob.png', '', '2018-10-18 07:56:04', NULL),
(23, '14213', NULL, 14211, 'Small', '<p>Black V Neck Cotton Jumper</p>', 79.00, '0', 'rx8dlf', '20', 0, '', '79.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9997, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233794blob.png', '', '2018-10-18 07:56:04', NULL),
(24, '14214', NULL, 14211, 'Large', '<p>Black V Neck Cotton Jumper</p>', 79.00, '0', 'isor6m', '20', 0, '', '79.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9992, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233794blob.png', '', '2018-10-18 07:56:04', NULL),
(25, '14215', NULL, 14211, 'X-Large', '<p>Black V Neck Cotton Jumper</p>', 79.00, '0', 'y7wmyp', '20', 0, '', '79.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9994, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233794blob.png', '', '2018-10-18 07:56:04', NULL),
(27, '14291', NULL, 14290, 'Small', '<p>Red marl crew neck jumper</p>', 110.00, '0', 'q4nuzc9', '20', 0, '', '110.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9999, 'n7omq9qno6nm4743vl3qgs4rb0inventory10231537423037blob.png', '', '2018-10-18 07:56:04', NULL),
(28, '14292', NULL, 14290, 'Medium', '<p>Red marl crew neck jumper</p>', 110.00, '0', 'cz4dlf', '20', 0, '', '110.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9999, 'n7omq9qno6nm4743vl3qgs4rb0inventory10231537423037blob.png', '', '2018-10-18 07:56:04', NULL),
(29, '14293', NULL, 14290, 'Large', '<p>Red marl crew neck jumper</p>', 110.00, '0', '994ujh', '20', 0, '', '110.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9999, 'n7omq9qno6nm4743vl3qgs4rb0inventory10231537423037blob.png', '', '2018-10-18 07:56:04', NULL),
(30, '14294', NULL, 14290, 'X-Large', '<p>Red marl crew neck jumper</p>', 110.00, '0', 'nvd55a', '20', 0, '', '110.00', '100.00', 'yes', '0.00', 0, '\"\"', '', '', 9999, 'n7omq9qno6nm4743vl3qgs4rb0inventory10231537423037blob.png', '', '2018-10-18 07:56:04', NULL),
(32, '14204', NULL, 14203, 'Small', '<p>Red V-Neck T-Shirt</p>', 20.00, '0', 'fc1sko', '20', 0, '', '20.00', '100.00', 'yes', '0.00', 0, '\"\"', '', 'dc143c', 99998, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233314blob.png', '', '2018-10-18 07:56:04', NULL),
(33, '14205', NULL, 14203, 'Medium', '<p>Red V-Neck T-Shirt</p>', 20.00, '0', 'slc86', '20', 0, '', '20.00', '100.00', 'yes', '0.00', 0, '\"\"', '', 'dc143c', 9998, '3auf9qbn74slu40nnpqv8qj3n0inventory10231537233314blob.png', '', '2018-10-18 07:56:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `venue_shops` longtext COLLATE utf8_unicode_ci,
  `store_news_id` int(11) DEFAULT '518',
  `user_id` bigint(20) DEFAULT NULL,
  `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenceNumber` int(11) DEFAULT NULL,
  `premises` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locality` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stateProvince` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Unknown',
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalCode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contactName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facsimile` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_information` text COLLATE utf8_unicode_ci,
  `pager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `snapchat_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_sender_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pointme_sender_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_sender_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue_description` text COLLATE utf8_unicode_ci,
  `venue_latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue_longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_onBoard` int(11) DEFAULT '0',
  `app_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#fff',
  `auto_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `beacon_condition` tinyint(1) NOT NULL DEFAULT '0',
  `minutes_condition` tinyint(1) NOT NULL DEFAULT '0',
  `beacon_area` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `beacon_listining` tinyint(1) NOT NULL DEFAULT '0',
  `beacon_minutes` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venue_app_skin`
--

CREATE TABLE `venue_app_skin` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `skin_title` varchar(200) NOT NULL,
  `json` text NOT NULL,
  `status` varchar(8) NOT NULL,
  `skin_image` varchar(100) NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_campaign_saturation`
--

CREATE TABLE `venue_campaign_saturation` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `saturation_alerts` varchar(255) NOT NULL,
  `saturation_block` varchar(255) NOT NULL,
  `contact_perioud_start_sms` varchar(255) NOT NULL,
  `contact_perioud_end_sms` varchar(255) NOT NULL,
  `contact_perioud_start_point_me` varchar(255) NOT NULL,
  `contact_perioud_end_point_me` varchar(255) NOT NULL,
  `compaing_satu_alerts` varchar(255) NOT NULL,
  `compaing_satu_block` varchar(255) NOT NULL,
  `rewards` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_categories`
--

CREATE TABLE `venue_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` text,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_cat_details`
--

CREATE TABLE `venue_cat_details` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_configurations_test_alerts`
--

CREATE TABLE `venue_configurations_test_alerts` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) DEFAULT NULL,
  `reporting_email` varchar(255) DEFAULT NULL,
  `recipient_email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `application_recipient` varchar(255) DEFAULT NULL,
  `mimimum_recipient_warning` varchar(255) DEFAULT NULL,
  `maximum_recipient_warning` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_details_flag`
--

CREATE TABLE `venue_details_flag` (
  `id` int(11) NOT NULL,
  `venue_image_flag` int(11) NOT NULL,
  `venue_address_flag` int(11) NOT NULL,
  `venue_map_flag` int(11) NOT NULL,
  `venue_trading_hours_falg` int(11) NOT NULL,
  `venue_phone_flag` int(11) NOT NULL,
  `venue_website_flag` int(11) NOT NULL,
  `venue_information_flag` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updatred_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_image`
--

CREATE TABLE `venue_image` (
  `venue_image_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `image` varchar(500) NOT NULL,
  `color` varchar(255) NOT NULL,
  `pay_with_points` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_operating_hours`
--

CREATE TABLE `venue_operating_hours` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `is_open` int(11) NOT NULL,
  `days` varchar(255) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_shops`
--

CREATE TABLE `venue_shops` (
  `id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_storages`
--

CREATE TABLE `venue_storages` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `venue_repo_size` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venue_subscriptions`
--

CREATE TABLE `venue_subscriptions` (
  `id` int(11) NOT NULL,
  `membership_id` varchar(255) NOT NULL,
  `persona_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_repositories`
--
ALTER TABLE `acl_repositories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_images`
--
ALTER TABLE `all_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `app_skinning`
--
ALTER TABLE `app_skinning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auto_checkout`
--
ALTER TABLE `auto_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auto_order_placement`
--
ALTER TABLE `auto_order_placement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `beacon_configurations`
--
ALTER TABLE `beacon_configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `venue_id` (`venue_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`);
ALTER TABLE `campaigns` ADD FULLTEXT KEY `description` (`description`);
ALTER TABLE `campaigns` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `campaign_logs`
--
ALTER TABLE `campaign_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_tags`
--
ALTER TABLE `campaign_tags`
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `campaign_id` (`campaign_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cms_levels`
--
ALTER TABLE `cms_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `credit_cards`
--
ALTER TABLE `credit_cards`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails_templates`
--
ALTER TABLE `emails_templates`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `favorite_products`
--
ALTER TABLE `favorite_products`
  ADD PRIMARY KEY (`favorite_product_id`);

--
-- Indexes for table `favourite_news`
--
ALTER TABLE `favourite_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_repositories`
--
ALTER TABLE `file_repositories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `repository_id` (`venue_id`);

--
-- Indexes for table `file_uploads`
--
ALTER TABLE `file_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fuel_stations`
--
ALTER TABLE `fuel_stations`
  ADD PRIMARY KEY (`fuel_station_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`group_member_id`);

--
-- Indexes for table `gyms`
--
ALTER TABLE `gyms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_category`
--
ALTER TABLE `image_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `knox_users`
--
ALTER TABLE `knox_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels_venues`
--
ALTER TABLE `levels_venues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level_configurations`
--
ALTER TABLE `level_configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loyalty_configurations`
--
ALTER TABLE `loyalty_configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lt_earn_velues`
--
ALTER TABLE `lt_earn_velues`
  ADD PRIMARY KEY (`lt_earn_velue_id`);

--
-- Indexes for table `lt_expiry_logs`
--
ALTER TABLE `lt_expiry_logs`
  ADD PRIMARY KEY (`lt_expiry_log_id`);

--
-- Indexes for table `lt_exp_points`
--
ALTER TABLE `lt_exp_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lt_points`
--
ALTER TABLE `lt_points`
  ADD PRIMARY KEY (`lt_point_id`);

--
-- Indexes for table `lt_redeem_values`
--
ALTER TABLE `lt_redeem_values`
  ADD PRIMARY KEY (`lt_redeem_value_id`);

--
-- Indexes for table `lt_role`
--
ALTER TABLE `lt_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `lt_role_assign`
--
ALTER TABLE `lt_role_assign`
  ADD PRIMARY KEY (`role_asg_id`);

--
-- Indexes for table `lt_rules`
--
ALTER TABLE `lt_rules`
  ADD PRIMARY KEY (`rule_id`);

--
-- Indexes for table `lt_rule_options`
--
ALTER TABLE `lt_rule_options`
  ADD PRIMARY KEY (`rule_option_id`);

--
-- Indexes for table `lt_rule_rewards`
--
ALTER TABLE `lt_rule_rewards`
  ADD PRIMARY KEY (`rule_reward_id`);

--
-- Indexes for table `lt_store_earn_velues`
--
ALTER TABLE `lt_store_earn_velues`
  ADD PRIMARY KEY (`lt_st_earn_velue_id`);

--
-- Indexes for table `lt_store_managers`
--
ALTER TABLE `lt_store_managers`
  ADD PRIMARY KEY (`lt_store_manager_id`);

--
-- Indexes for table `lt_tiers`
--
ALTER TABLE `lt_tiers`
  ADD PRIMARY KEY (`lt_tier_id`);

--
-- Indexes for table `lt_tier_rules`
--
ALTER TABLE `lt_tier_rules`
  ADD PRIMARY KEY (`lt_tier_rule_id`);

--
-- Indexes for table `lt_transations`
--
ALTER TABLE `lt_transations`
  ADD PRIMARY KEY (`lt_transation_id`);

--
-- Indexes for table `lt_user_tier`
--
ALTER TABLE `lt_user_tier`
  ADD PRIMARY KEY (`lt_user_tier_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `news_news_category_id_foreign` (`news_category_id`);

--
-- Indexes for table `news_categories`
--
ALTER TABLE `news_categories`
  ADD PRIMARY KEY (`news_category_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `offer_categories`
--
ALTER TABLE `offer_categories`
  ADD PRIMARY KEY (`offer_category_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `option_sets`
--
ALTER TABLE `option_sets`
  ADD PRIMARY KEY (`option_set_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`order_detail_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`pm_id`);

--
-- Indexes for table `point_type`
--
ALTER TABLE `point_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pos`
--
ALTER TABLE `pos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pos_mapper`
--
ALTER TABLE `pos_mapper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`prod_cat_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_option_set`
--
ALTER TABLE `product_option_set`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `product_promotion`
--
ALTER TABLE `product_promotion`
  ADD PRIMARY KEY (`pp_id`);

--
-- Indexes for table `product_variant`
--
ALTER TABLE `product_variant`
  ADD PRIMARY KEY (`pv_id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`promotion_id`);

--
-- Indexes for table `punch_cards`
--
ALTER TABLE `punch_cards`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `punch_cards` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe_category`
--
ALTER TABLE `recipe_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe_ingredients`
--
ALTER TABLE `recipe_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe_offers`
--
ALTER TABLE `recipe_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe_preparation`
--
ALTER TABLE `recipe_preparation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe_reviews`
--
ALTER TABLE `recipe_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe_tags`
--
ALTER TABLE `recipe_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repositories`
--
ALTER TABLE `repositories`
  ADD PRIMARY KEY (`repository_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_acls`
--
ALTER TABLE `roles_acls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_assigns`
--
ALTER TABLE `role_assigns`
  ADD PRIMARY KEY (`role_assign_id`);

--
-- Indexes for table `role_level_users`
--
ALTER TABLE `role_level_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `segment`
--
ALTER TABLE `segment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `venue_id` (`venue_id`);
ALTER TABLE `segment` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `segment` ADD FULLTEXT KEY `description` (`description`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `sp_transations`
--
ALTER TABLE `sp_transations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storages`
--
ALTER TABLE `storages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_images`
--
ALTER TABLE `store_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_information`
--
ALTER TABLE `store_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_products`
--
ALTER TABLE `store_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_settings`
--
ALTER TABLE `store_settings`
  ADD PRIMARY KEY (`store_setting_id`);

--
-- Indexes for table `swift_country`
--
ALTER TABLE `swift_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `swift_family`
--
ALTER TABLE `swift_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `swift_group`
--
ALTER TABLE `swift_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `swift_media`
--
ALTER TABLE `swift_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `tags` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_cards`
--
ALTER TABLE `user_cards`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `user_redeemed_offers`
--
ALTER TABLE `user_redeemed_offers`
  ADD PRIMARY KEY (`user_redeemed_offer_id`);

--
-- Indexes for table `user_vouchers`
--
ALTER TABLE `user_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variant`
--
ALTER TABLE `variant`
  ADD PRIMARY KEY (`variant_id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `venue_id` (`venue_id`),
  ADD KEY `store_news_id` (`store_news_id`),
  ADD KEY `user_id` (`user_id`);
ALTER TABLE `venues` ADD FULLTEXT KEY `venue_name` (`venue_name`);

--
-- Indexes for table `venue_app_skin`
--
ALTER TABLE `venue_app_skin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_campaign_saturation`
--
ALTER TABLE `venue_campaign_saturation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_categories`
--
ALTER TABLE `venue_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_cat_details`
--
ALTER TABLE `venue_cat_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_configurations_test_alerts`
--
ALTER TABLE `venue_configurations_test_alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_image`
--
ALTER TABLE `venue_image`
  ADD PRIMARY KEY (`venue_image_id`);

--
-- Indexes for table `venue_operating_hours`
--
ALTER TABLE `venue_operating_hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_shops`
--
ALTER TABLE `venue_shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_storages`
--
ALTER TABLE `venue_storages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_subscriptions`
--
ALTER TABLE `venue_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_repositories`
--
ALTER TABLE `acl_repositories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `all_images`
--
ALTER TABLE `all_images`
  MODIFY `image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `app_skinning`
--
ALTER TABLE `app_skinning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auto_checkout`
--
ALTER TABLE `auto_checkout`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auto_order_placement`
--
ALTER TABLE `auto_order_placement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `beacon_configurations`
--
ALTER TABLE `beacon_configurations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaign_logs`
--
ALTER TABLE `campaign_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT for table `cms_levels`
--
ALTER TABLE `cms_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credit_cards`
--
ALTER TABLE `credit_cards`
  MODIFY `card_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `emails_templates`
--
ALTER TABLE `emails_templates`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorite_products`
--
ALTER TABLE `favorite_products`
  MODIFY `favorite_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourite_news`
--
ALTER TABLE `favourite_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file_repositories`
--
ALTER TABLE `file_repositories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file_uploads`
--
ALTER TABLE `file_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fuel_stations`
--
ALTER TABLE `fuel_stations`
  MODIFY `fuel_station_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_members`
--
ALTER TABLE `group_members`
  MODIFY `group_member_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gyms`
--
ALTER TABLE `gyms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image_category`
--
ALTER TABLE `image_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `knox_users`
--
ALTER TABLE `knox_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `levels_venues`
--
ALTER TABLE `levels_venues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level_configurations`
--
ALTER TABLE `level_configurations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loyalty_configurations`
--
ALTER TABLE `loyalty_configurations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_earn_velues`
--
ALTER TABLE `lt_earn_velues`
  MODIFY `lt_earn_velue_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_expiry_logs`
--
ALTER TABLE `lt_expiry_logs`
  MODIFY `lt_expiry_log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_exp_points`
--
ALTER TABLE `lt_exp_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_points`
--
ALTER TABLE `lt_points`
  MODIFY `lt_point_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_redeem_values`
--
ALTER TABLE `lt_redeem_values`
  MODIFY `lt_redeem_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_role`
--
ALTER TABLE `lt_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_role_assign`
--
ALTER TABLE `lt_role_assign`
  MODIFY `role_asg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_rules`
--
ALTER TABLE `lt_rules`
  MODIFY `rule_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_rule_options`
--
ALTER TABLE `lt_rule_options`
  MODIFY `rule_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_rule_rewards`
--
ALTER TABLE `lt_rule_rewards`
  MODIFY `rule_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_store_earn_velues`
--
ALTER TABLE `lt_store_earn_velues`
  MODIFY `lt_st_earn_velue_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_store_managers`
--
ALTER TABLE `lt_store_managers`
  MODIFY `lt_store_manager_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_tiers`
--
ALTER TABLE `lt_tiers`
  MODIFY `lt_tier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_tier_rules`
--
ALTER TABLE `lt_tier_rules`
  MODIFY `lt_tier_rule_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_transations`
--
ALTER TABLE `lt_transations`
  MODIFY `lt_transation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lt_user_tier`
--
ALTER TABLE `lt_user_tier`
  MODIFY `lt_user_tier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_categories`
--
ALTER TABLE `news_categories`
  MODIFY `news_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offer_categories`
--
ALTER TABLE `offer_categories`
  MODIFY `offer_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `point_type`
--
ALTER TABLE `point_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `product_variant`
--
ALTER TABLE `product_variant`
  MODIFY `pv_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `punch_cards`
--
ALTER TABLE `punch_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_category`
--
ALTER TABLE `recipe_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_ingredients`
--
ALTER TABLE `recipe_ingredients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_offers`
--
ALTER TABLE `recipe_offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_preparation`
--
ALTER TABLE `recipe_preparation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_reviews`
--
ALTER TABLE `recipe_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recipe_tags`
--
ALTER TABLE `recipe_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_assigns`
--
ALTER TABLE `role_assigns`
  MODIFY `role_assign_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `segment`
--
ALTER TABLE `segment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_cards`
--
ALTER TABLE `user_cards`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_redeemed_offers`
--
ALTER TABLE `user_redeemed_offers`
  MODIFY `user_redeemed_offer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `variant`
--
ALTER TABLE `variant`
  MODIFY `variant_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_app_skin`
--
ALTER TABLE `venue_app_skin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_campaign_saturation`
--
ALTER TABLE `venue_campaign_saturation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_categories`
--
ALTER TABLE `venue_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_cat_details`
--
ALTER TABLE `venue_cat_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_configurations_test_alerts`
--
ALTER TABLE `venue_configurations_test_alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_image`
--
ALTER TABLE `venue_image`
  MODIFY `venue_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_operating_hours`
--
ALTER TABLE `venue_operating_hours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_shops`
--
ALTER TABLE `venue_shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_storages`
--
ALTER TABLE `venue_storages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venue_subscriptions`
--
ALTER TABLE `venue_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
