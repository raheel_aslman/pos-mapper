<?php

namespace App\Providers;

use App\Traits\MultiLogsTraits;
use Illuminate\Support\ServiceProvider;

class MultiLogServiceProvider extends ServiceProvider
{
    use MultiLogsTraits;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCustomLoggers();
    }


}
