<?php

namespace App\interfaces;
/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/14/2017
 * Time: 3:22 PM
 */

interface PosMapper{

    public static function getMappedData($serviceId);

    public static function mapFields($receivedData, $fields);
}