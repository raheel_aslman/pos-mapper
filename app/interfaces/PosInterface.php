<?php

namespace App\interfaces;

/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/14/2017
 * Time: 4:01 PM
 */

interface PosInterface{

    public function saveStoreData( $params = [] );

    public function saveCategoryData( $params = [] );

    public function saveProductsData( $params = [] );

    public function getStore( $params = [] );


    public function saveOptionSetData( $params = [] );

    public function saveVariantData( $params = [] );

    public function getMappers( );

}
