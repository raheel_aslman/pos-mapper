<?php
namespace App\PosOrders;

use App\ApiCalls\StarrtecPosApiCall;
use App\Facade\StarrtecLog;
use App\Models\Customer;
use App\Models\Optionset;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Product;
use App\Models\Store;
use App\Models\Variant;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 7/7/2017
 * Time: 10:16 AM
 */
class StarrtecPosOrder extends BasePosOrder
{
    private $faker;
    private $apiCall;

    /**
     * StarrtecPosOrder constructor.
     */
    public function __construct()
    {
        $this->apiCall = new StarrtecPosApiCall();
        $this->faker = Faker::create();
    }//..... end of __construct() .....//

    /**
     * @param $locationID
     * Place order
     */
    public function placeOrder($locationID)
    {
        $order = $this->createOrder($locationID);
        StarrtecLog::info("  ".basename(__FILE__)."  Sending Order Details to Starrtec POS Order Api.");
        $response = $this->apiCall->placeOrder($order, $locationID);
        StarrtecLog::info("  ".basename(__FILE__)."  Api response returned for Order from Starrtec POS order api.");

        StarrtecLog::info("  ".basename(__FILE__)."  Updating Order Status in Unified Database.");
        return $this->updateOrderStatus($response);
    }//..... end of placeOrder() ......//

    /**
     * create order details.
     */
    private function createOrder($locationID)
    {
        return $this->getLocationProduct($locationID);
    }//.... end of createOrder() ......//

    /**
     * create fake user.
     */
    private function getFakeUser()
    {
        $user = [
            "id"=>NULL,
            "name"=> $this->faker->name,
            "phone"=> $this->faker->phoneNumber,
            "email"=> $this->faker->email,
            "address"=>[
                "line1"=> $this->faker->address,
                "line2"=> $this->faker->streetAddress,
                "town"=> $this->faker-> streetName,
                "postcode"=> $this->faker->postcode,
                "state"=> $this->faker->city,
                "country"=> $this->faker->country
            ]
        ];

        $customer = Customer::create(['name'=> $user['name'],"phone"=> $user['phone'], "email"=> $user['email'],"address_line1"=> $user['address']['line1'],
            "address_line2"=> $user['address']['line2'],"town"=> $user['address']['town'],"postcode"=> $user['address']['postcode'],"country"=> $user['address']['country']]);

        $user['id'] = $customer->id;

        return $user;
    }//..... end of getFakeUser() .......//

    /**
     * @param $locationID
     * Get dummy location product.
     */
    private function getLocationProduct($locationID)
    {
        $store = Store::where("pos_store_id", $locationID)->first();
        //..... get products.
        $productsArray = [];
        $products = Product::where(["pos_code"=> 2, 'store_id'=> $store->store_id])->orderBy(DB::raw("RAND()"))->take(rand(2,6))->get()
            ->each(function($record)use($store,&$productsArray){

                $optionsSet = [];
                Optionset::where(['option_sets.pos_code'=> 2, 'option_sets.store_id'=> $store->store_id, "product_option_set.product_id"=> $record->product_id])
                    ->join("product_option_set","option_sets.option_set_id","=","product_option_set.option_set_id")->get()
                    ->each(function($opt)use(&$optionsSet,$record){
                        $variants = [];
                        Variant::where(['product_variant.product_id'=> $record->product_id])
                            ->join("product_variant","product_variant.variant_id","=","variant.variant_id")->get()
                            ->each(function($vrt)use(&$variants){
                                $variants[] =(object) [
                                    "Id"=> $vrt->pos_variant_id,
                                    "Name"=> $vrt->variant_name,
                                    "Price"=> $vrt->variant_price
                                ];
                            });
                        $optionsSet[] = (object) [
                            "Id"=> $opt->pos_option_id,
                            "Name"=> $opt->option_set_name,
                            "Minimum"=> $opt->min_selection,
                            "Maximum"=> $opt->max_selection,
                            "Variants"=> $variants
                        ];

                    });

                $productsArray[] = (object)[
                    "Id" => $record->pos_product_id,
                    "Name"=> $record->product_name,
                    "Description"=> $record->product_description,
                    "Price"=> $record->product_price,
                    "Options"=> $optionsSet,
                    "Promotions"=>[(object)[
                        "Id"=>rand(1,129),
                        "Name"=> $this->faker->sentence(2),
                        "PromotionType"=> "discount",
                        "Amount"=> rand(1,5),
                        "Value"=> rand(1,6)
                    ]]
                ];
            });

        return $this->saveOrder($products,$productsArray);
    }//..... end of getLocationProduct() ......//

    /**
     * @param $products
     * Save Order to db
     */
    private function saveOrder($products, $productsArray)
    {
        StarrtecLog::info("  ".basename(__FILE__)."  Saving Order Details to Unified Database.");
        $user = $this->getFakeUser();
        $body = [
            "orderId" => NULL,
            "orderType" => array_rand(['pickup','delivery','dinein']),
            "notes"     => $this->faker->text(),
            "requiredAt"    => Carbon::now()->format("m/d/Y H:i:s"),
            "user"          => json_decode(json_encode($user)),
            "promotions"    =>[(object)[
                "Id"=> rand(1,200),
                "Name"=> $this->faker->word,
                "PromotionType"=>"value",
                "Amount"=> rand(1,20),
                "Value"=> rand(1,20)
            ]],
            "products"      => $productsArray,
            "Payments"=> [(object)[
                "Id"=> "1",
                "Name"=> "Cash",
                "Amount"=> 0,
                "Ref"=> "manual"
            ]]
        ];

        $venue = [180,183,184,59];
        $order = Order::create(["order_type"=> $body["orderType"], "order_note"=> $body['notes'],"customer_id"=> $user['id'],"payment_method_id"=> 1,"venue_id"=>$venue[array_rand($venue,1)],"pos_code"=> 2]);
        $body['orderId'] = $order->order_id;

        //..... place order details
        $amount = 0;
        foreach ($products as $product){
            $amount += $product->product_price;
            OrderDetails::create(['order_id'=> $order->order_id, "product_id"=> $product->product_id, "unit_price"=> $product->product_price,"quantity"=> rand(10,30)]);
        }
        StarrtecLog::info("  ".basename(__FILE__)."  Order Details Saved to Unified Database.");

        $body['Payments'][0]->Amount = $amount;
        return $body;
    }//..... end of saveOrder() ......//

    private function updateOrderStatus($response)
    {
        Order::where("order_id",$response['orderId'])->update(['order_status'=> 1]);
        StarrtecLog::info("  ".basename(__FILE__)."  Order Status Updated in Unified Database.");
        echo "Order Placed Successfully. Order Response Data is Below";

        dd($response);
    }//..... end of updateOrderStatus() ......//
}//..... end of class.