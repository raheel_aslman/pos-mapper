<?php
namespace App\ApiCalls;

/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/15/2017
 * Time: 11:42 AM
 */

use App\Facade\StarrtecLog;

class StarrtecPosApiCall extends BasePosApiCall
{
    private $location_url;
    private $header = [];

    /**
     * StarrtecPosApiCall constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->header       = ["Content-Type" => "application/json",'Authorization' => config('constants.STARRTEC_TOKEN')];
        $this->location_url = config("constants.STARRTEC_URL");
    }//..... end of constructor() ......//

    /**
     * @return array
     * Get Locations from Starrtec Apis.
     */
    public function getLocations()
    {
        StarrtecLog::info("  ".basename(__FILE__)."  Fetching Locations Data from Starrtec POS REST Endpoint ".$this->location_url);

        try{
            StarrtecLog::info("  ".basename(__FILE__)."  Result returned from Starrtec POS REST Endpoint.");
            return $this->get($this->location_url,$this->header);
        }catch(\Exception $e) {
            StarrtecLog::error("  ".basename(__FILE__)."  Error Occurred at Starrtec REST Endpoint. ".$e->getMessage());
            return false;
        }//..... end of try-catch() block ......//
    }//..... end of getLocations() .....//

    /**
     * @param $location_id
     * @return bool|string
     * Get Single Location by id.
     */
    public function getSingleLocation($location_id)
    {
        StarrtecLog::info("  ".basename(__FILE__)."  Calling Locations' menus API of the Starrtec POS.");

        $url = str_replace('?',"/".$location_id."?",$this->location_url);

        try{
            StarrtecLog::info("  ".basename(__FILE__)."  Result returned from Locations' Menus API of the Starrtec POS.");
            return $this->get($url,$this->header);
        }catch(\Exception $e) {
            StarrtecLog::warning("  ".basename(__FILE__)." ".$e->getMessage());
            return false;
        }//..... end of try-catch() block ......//
    }//..... end of getSingleLocation() ......//

    /**
     * @param $location_id
     * @return bool|string
     * Get Specific Location Details.
     */
    public function getLocationData($location_id)
    {
        StarrtecLog::info("  ".basename(__FILE__)."  Calling Locations' menus API of the Starrtec POS.");

        $url = str_replace('?',"/".$location_id."/menus?",$this->location_url);

        try{
            StarrtecLog::info("  ".basename(__FILE__)."  Result returned from Locations' Menus API of the Starrtec POS.");
            return $this->get($url,$this->header);
        }catch(\Exception $e) {
            StarrtecLog::warning("  ".basename(__FILE__)." ".$e->getMessage());
            return false;
        }//..... end of try-catch() block ......//
    }//..... end of getLocationData() ......//

    /**
     * Get Multi Location data.
     */
    public function getMultiLocationData($locationIDS)
    {
        $request = [];
        foreach ($locationIDS as $location_id):
            $rh['url'] = str_replace('?',"/".$location_id."/menus?",$this->location_url);
            $rh['header'] = $this->header;
            $request[] = $rh;
        endforeach;

        StarrtecLog::info("  ".basename(__FILE__)."  Getting Locations' details of the Starrtec POS REST using Multi Request.");
        try{
            StarrtecLog::info("  ".basename(__FILE__)."  Result returned from Locations' Menus API of the Starrtec POS.");
            return $this->getMulti($request);
        }catch(\Exception $e) {
            StarrtecLog::error("  ".basename(__FILE__)."  Getting Locations' details returned error. ".$e->getMessage());
            return false;
        }//..... end of try-catch() block ......//
    }//..... end of getMultiLocationData() ......//

    /**
     * @param $body
     * Place order.
     */
    public function placeOrder($order, $locationID)
    {
        return $order;
        $url = str_replace('?',"/".$locationID."/orders?",$this->location_url);
        $header = $this->header;
        unset($header['Content-Type']);

       $response = $this->post($url, $header, $order);
    }
}