<?php
namespace App\ApiCalls;
use App\ApiCalls\BasePosApiCall;
use App\Facade\SoldiLog;


class SoldiPosApiCall extends BasePosApiCall
{
    
    private $header = [];
    
    public function __construct() {
        parent::__construct();
        $this->header = ['X-API-KEY' => config("constants.SOLDI_APIKEY"),'SECRET' => config("constants.SOLDI_SECRETKEY")];

   }
   
    protected $fileName = 'SoldiPosApiCall.php';


    public function getAllStores()
    {
        $url = config('constants.SOLDIURL')."business/store";
    
        try{
             
            $res = $this->get($url,$this->header);
            return $res;
        }catch(\Exception $e)
        {
            SoldiLog::warning($this->fileName."  ".$e->getMessage());
        }
    }
    
    
    
    public function getStore($apiKey="" , $secretKey="" , $storeId)
    {
        $url = config('constants.SOLDIURL')."business/store";
        //$url    = config('constants.SOLDIURL')."business/detail?business_id=".$storeId;
        try{
             
            $res = $this->get($url,$this->header);
            return $res;
        }catch(\Exception $e)
        {
            SoldiLog::warning($this->fileName."  ".$e->getMessage());
        }
    }
    
     public function getCategories($apiKey , $secretKey , $storeId)
    {
         
        $url    = config('constants.SOLDIURL')."pos/category?business_id=".$storeId;
        $header =  ['X-API-KEY' => $apiKey,'SECRET' => $secretKey];
    
        try{
            $res = $this->get($url,$header);
            return $res;
        }catch(\Exception $e)
        {
             SoldiLog::warning($this->fileName."  ".$e->getMessage());
        }
    }
    
    public function getProducts($categories)
    {
        
        $requestData = [];
        
        $i = 0;
        foreach($categories as $cat)
        {
            $url = config('constants.SOLDIURL')."inventory/listproducts?"
                    . "business_id=".$cat["pos_store_id"]."&cate_id=".$cat["pos_cat_id"];
            $header =  ['X-API-KEY' => $cat["apiKey"],'SECRET' => $cat["secretKey"]];
            $requestData[$i]['header'] = $header;
            $requestData[$i]['url']    = $url;
            $i++;
        }
        
        
        //dd($requestData);
        try{
            $res = $this->getMulti($requestData);
            return $res;
        }catch(\Exception $e)
        {
            SoldiLog::warning($this->fileName."  ".$e->getMessage());
        }
    }
    
    
    /**
     * get Multiple Category
     * **/
    
    public function getMultiCategories($stores)
    {
      
    $requestData = [];

    $i = 0;
    foreach($stores as $store)
    {
        $url    = config('constants.SOLDIURL')."pos/category?business_id=".$store["business_id"];
        $header =  ['X-API-KEY' => $store["business_api_key"],'SECRET' => $store["business_secret"]];
        $requestData[$i]['header'] = $header;
        $requestData[$i]['url']    = $url;
        $i++;
    }
    //dd($requestData);
    try{
        $res = $this->getMulti($requestData);
        return $res;
    }catch(\Exception $e)
        {
        SoldiLog::warning($this->fileName."  ".$e->getMessage());
        }
    }
    
    
}

