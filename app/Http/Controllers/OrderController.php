<?php

namespace App\Http\Controllers;

use App\Facade\StarrtecLog;
use App\PosOrders\StarrtecPosOrder;

class OrderController extends Controller
{

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        //
    }//..... end of __construct() ......//

    /**
     * Function for placing order entry point.
     */
    public function placeOrder()
    {
        $this->placeOrderAtStarrtecPos(2495);
    }//..... end of placeOrder() .......//

    /**
     * @param $locationID
     * Place order at Starrtecc Pos.
     */
    private function placeOrderAtStarrtecPos($locationID)
    {
        StarrtecLog::info("  ".basename(__FILE__)."  Placing Order at Starrtec for Store ID:".$locationID.".");
        $starrtecPosOrder = new StarrtecPosOrder();
        return $starrtecPosOrder->placeOrder($locationID);
    }//..... end of placeOrderAtStarrtecPos() .......//


}//...... end of class.
