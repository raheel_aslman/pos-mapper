<?php

namespace App\Http\Controllers;

use App\Traits\initializeAutoIDTraits;
use Illuminate\Http\Request;

use App\interfaces\PosInterface;


abstract class BasePosController extends Controller implements PosInterface
{
    use initializeAutoIDTraits;
}
