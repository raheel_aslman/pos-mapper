<?php

namespace App\Http\Controllers;

use App\ApiCalls\StarrtecPosApiCall;
use App\Models\Pos;
use Illuminate\Http\Request;

class CroneJobController extends Controller
{

    /**
     * CroneJobController constructor.
     */
    public function __construct()
    {
        //
    }//..... end of __construct() ......//

    /**
     * Daily Crone Job.....
     */
    public function dailyCroneJob()
    {
        //$this->getPosData();
        $this->populateStarrtecData();
    }//..... end of dailyCroneJob() ......//

    /**
     * Get Updated Data from all POSs.
     */
    private function getPosData()
    {
        // get all POSs and run job for each accordingly.
        //$poss = Pos::all();
//        foreach ($poss as $key => $pos):
//            if($pos->id == 1)// update soldi data.
                (new SoldiPosController())->populateData();
//        endforeach;
    }//.... end of getPosData() .....//

    /**
     * update the starrtec data.
     */
    private function populateStarrtecData()
    {
        (new StarrtecposController())->populateData();
    }//..... end of populateStarrtecData() ......//
}
