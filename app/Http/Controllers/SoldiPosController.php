<?php

/**
 * File Summary
 */

namespace App\Http\Controllers;

use App\ApiCalls\BasePosApiCall;
use App\ApiCalls\SoldiPosApiCall;
use App\Facade\SoldiLog;
use App\mapper\BasePosMapper;
use App\mapper\SoldiPosMapper;
use Illuminate\Http\Request;
use Response;
use Carbon\Carbon;
use App\Models\Store;
use App\Models\Category;
use App\Models\Product;
use App\Models\productImages;
use App\Models\Variant;
use App\Models\ProductVariant;
use App\Models\Optionset;
use App\Models\Options;
use App\Models\ProductOption;
use DB;


/**
 * Home Controller
 */
class SoldiPosController extends BasePosController {

    private $apiCall ;
    private $fileName;
    private $mapper;
    private $pos_code;
    private $stores = 0,$categories = 0, $products = 0, $optionset = 0, $variants = 0, $options = 0;
    private $store_mapper,$category_mapper,$product_mapper,$variant_mapper,$optionset_mapper,$option_mapper;
    private $store_ids = [];
    private $storeAutoId, $categoryId, $productId, $optionSetId, $variantId,$optionsId;
    private $StoreData          = [],
            $categoryData       = [],
            $productData        = [],
            $optionSetData      = [],
            $variantData        = [],
            $productVariantData = [],
            $optionsData        = [],
            $productOptionData  = [];
    
    //use initializeAutoIDTraits;


    
    public function __construct() {
       
        
        $this->apiCall  = new SoldiPosApiCall();
        $this->mapper   = new SoldiPosMapper();
        $this->pos_code = config('serviceConstants.SOLDI_POS_CODE');
        $this->getMappers();
        $this->fileName = 'SoldiPosController.php';
    }// end of __construct
    
    
    
    /**
     * @return void
     * This function populate all data for Soldi API.
     */
    public function populateData() {
        
        $batchId =  generateBatchId();
        $this->printLogHeader($batchId);

        $this->saveStoreData(['response' => $this->apiCall->getStore($apiKey="MjcwOWM4YmFjNTA0NjAxNjVlZTBhOD" , $secretKey="MTI2NjZkNjgwMjNjYTJkYTkxOTQ3OT" , $storeId=0)]);
        $this->printLogFooter($batchId);
        $this->saveData();
        return "Stores: {$this->stores}, Categories: {$this->categories}, Products: {$this->products}, Options: {$this->optionset}, Variants: {$this->variants} Saved Successfully.";
       
    }// end populateData
    
    /**
     * This function used to show the log information when first time send the request to populate function.
     */
    public function printLogHeader($batchId) {
        
        SoldiLog::info("************************************************************************************************************************");
        SoldiLog::info("************************************************************************************************************************");
        SoldiLog::info("************************  Fetching Data From SOLDI POS against Batch ID [".$batchId."] Started  ********************");
        SoldiLog::info("************************************************************************************************************************");
        SoldiLog::info("************************************************************************************************************************");
        SoldiLog::info("  ".basename(__FILE__)."  Soldi function populateData Call");
        
    }// end initialLogInformation
    
    
    /**
     * This function used to print the log footer
     */
    public function printLogFooter($batchId) {
        
        SoldiLog::info("                                        ########################################                        ");
        SoldiLog::info("  ".basename(__FILE__)."             No of Stores       Fetched : ".$this->stores."               ");
        SoldiLog::info("  ".basename(__FILE__)."             No of Categories   Fetched : ".$this->categories."           ");
        SoldiLog::info("  ".basename(__FILE__)."             No of Products     Fetched : ".$this->products."             ");
        SoldiLog::info("                                        ########################################                        ");
        SoldiLog::info("**************************************************************************************************************************");
        SoldiLog::info("**********************  Fetching Data From SOLDI POS APIs against Batch ID [".$batchId."] Finished  ******************");
        SoldiLog::info("**************************************************************************************************************************");       
        
    }// end endLogInformation
    
    
    /**
     * @param    $apiKey , $secretKey, $storeId,.
     * This function used to get store data from soldi and save to database
     */
    public function getStore($params = []) {
        
    }
    
    /**
     * @param json $response.
     * @return void
     * This function used to map and save soldi store data to database
     */
    public function saveStoreData($params = []) {

        extract($params);

        if ($response) {

            $responseArr = json_decode($response, true);
            $storeInfo = $responseArr['data'];
            $this->storeAutoId = $this->initializeStoreAutoIncrementID(count($storeInfo));
            $total_insert = 0;
            if(count($storeInfo) > 0){
                foreach ($storeInfo as $key => $sinfo) {
                    $data = $this->mapper->mapFields((array)$sinfo, (array) json_decode($this->store_mapper,true));
                    $this->StoreData[] = array_merge($data,["store_id"=> $this->storeAutoId ,'pos_code'=> $this->pos_code,'created_at'=> Carbon::now()]);
                    SoldiLog::info("  ".basename(__FILE__)."  saving store data to unified database table store");
//                    $inserted = Store::create($saveData);
//                    $this->store_ids[$saveData['pos_store_id']] = $inserted->store_id;

                    $storeInfo[$key]["store_id"]     = $this->storeAutoId;
                    $storeInfo[$key]["pos_store_id"] = $data['pos_store_id'];
                    $this->stores += 1;
                    ++$this->storeAutoId;
                    $storeId   = $sinfo["business_id"];
                    $apiKey    = $sinfo["business_api_key"];
                    $secretKey = $sinfo["business_secret"];
                    //$this->getProducts($apiKey , $secretKey , $storeId);
                    $total_insert++;

                }

            }
            $this->getCategories($storeInfo);
            if ($total_insert > 0) {
                SoldiLog::info("  ".basename(__FILE__)."  Total Stores inserted: . $total_insert");
            } else {
                SoldiLog::error("  ".basename(__FILE__)."  No data save for store");
            }

        } else {
            SoldiLog::error("  ".basename(__FILE__)."  No data save for store. Response not received");
        }
    }
    
    

    /**
     * @param int $storeInfo.
     * @return void
     * This function used to get category data from soldi and save to database
     */
    
    public function getCategories($storeInfo) {
        
        $result = $this->apiCall->getMultiCategories($storeInfo);
        foreach ($storeInfo as $key => $sInfo){
            $storeInfo[$key]["Categories"] = $result[$key];
        }//..... end foreach() .....//
        $this->saveCategoryData(["storeInfo" => $storeInfo]);
        SoldiLog::info("  ".basename(__FILE__)."  Soldi get Categories api Call for storeID:1");
    }
    
    
    /**
     *@param json $response ,$storeId.
     *@return void
     * This function used to map and save soldi category data to database
     */    
    public function saveCategoryData($params = []) {
        
        extract($params);
        if(count($storeInfo)){
            $apiCatArr = [];
            
            foreach($storeInfo as $catInfo){
                
            $categories = (json_decode($catInfo["Categories"]))->data;
            $this->categoryId = $this->initializeCategoryAutoIncrementID(count($categories));
            if(count($categories) > 0){
                foreach ($categories as $category) {
                    
                    $apiCatArr[] = ['apiKey'=> $catInfo["business_api_key"],'secretKey'=> $catInfo["business_secret"],"pos_store_id"=> $catInfo["pos_store_id"],"store_id"=> $catInfo["store_id"], "pos_cat_id"=> $category->cate_id,"category_id"=>$this->categoryId];
                    $data = $this->mapper->mapFields((array)$category, (array) json_decode($this->category_mapper));
                    $this->categoryData[] = array_merge($data,['pos_code'=> $this->pos_code,"store_id"=> $catInfo["store_id"], "category_id"=> $this->categoryId,'created_at'=> Carbon::now()]);
                    $this->categories += 1;

                    SoldiLog::info("  ".basename(__FILE__)."  Category with id: ".$this->categoryId." prepared for Saving.");
                    ++$this->categoryId;
                    }//..... end of foreach() .....//                    
                }
            }

            if(count($apiCatArr) > 0){
                $this->getProducts($apiCatArr);
            }
            }else{
            SoldiLog::error("  ".basename(__FILE__)."  No data save for categories. Response not received");
        }  
    }
    
    
     /**
     *@param json $storeId.
     *@return void
     * This function used to get products data to database
     */
    
    public function getProducts($catInfo)
    {
        
        $result   = $this->apiCall->getProducts($catInfo);
        if($result){
            foreach($catInfo as $key => $cInfo){
                $catInfo[$key]["Products"] = $result[$key];
            }
        }
         $this->saveProductsData(["productsInfo" => $catInfo]);
        SoldiLog::info("  ".basename(__FILE__)."  Soldi get Products api Call for storeID:1");
    }
    
    /**
     *@param json $response ,$storeId.
     *@return void
     * This function used to map and save soldi products data to database
     */
    public function saveProductsData($params = []) {
        
        
        extract($params);
        $variants_array       = [];
        $optionset_array      = [];

        if(count($productsInfo) > 0){
            
            foreach($productsInfo as $prodInfo){
                $products = (json_decode($prodInfo["Products"]))->data;
                $this->productId = $this->initializeProductAutoIncrementID(count($products));

                if(count($products) > 0){
                    foreach($products as $key=>$pro){
                        if(!empty($pro->variants_array)){
                            $variants_array           = $pro->variants_array;
                            $final_variants_array[]   = array_merge($variants_array,["product_id"=> $this->productId]);
                        }
                        if(!empty($pro->modifiers_array)){
                            $optionset_array         = $pro->modifiers_array;
                            $final_optionset_array[] = array_merge($optionset_array,["product_id"=> $this->productId,"store_id"=>$prodInfo["store_id"]]);
                        }
                        $data = $this->mapper->mapFields((array)$pro, (array) json_decode($this->product_mapper));
                        $this->productData[] = array_merge($data,["category_id"=> $prodInfo["category_id"], 'pos_code'=> $this->pos_code,"store_id"=> $prodInfo["store_id"], "product_id"=> $this->productId,'created_at'=> Carbon::now()]);
                        SoldiLog::info("  ".basename(__FILE__)."  Product with id: ".$this->productId.", of Category id: ".$prodInfo["category_id"]." is prepared for Saving.");
                        ++$this->productId;
                         $this->products += 1;
                    }
                    
                }
            
            }

           $this->saveProductVariants(["variants_array"=>@$final_variants_array]);
           $this->saveProductOptionSet(["optionset_array"=>@$final_optionset_array]);
           
            
        }
         
//        if(count($responses) > 0):
//        foreach($responses as $response):
//            
//            $responseArr = json_decode($response, true);
//            $productsInfo = $responseArr['data'];
//
//            $mapDataFields = json_decode($this->product_mapper, true);
//
//            foreach ($productsInfo as $prodInfo) :
//                        
//                        $saveData = BasePosMapper::mapFields($prodInfo, $mapDataFields);
//                        $saveData['pos_code'] = 1;
//                        $saveData['store_id'] = $this->store_ids[$storeId];
//                        SoldiLog::info("  ".basename(__FILE__)."  Soldi Pos product data inserted");
//                        $product = Product::create($saveData);
//                       
//                        if ($product->product_id > 0) {
//                            $this->saveProductImages($prodInfo['prd_image'],$product->product_id);
//                            $this->saveProductVariants($prodInfo['variants_array'],$product->product_id);
//                            $this->saveProductOptionSet($prodInfo['modifiers_array'],$product->product_id,$storeId);
//                        }
//                        
//                       $this->products++;
//                        
//            endforeach;
//            
//        endforeach;
//        
//        endif;
        

    }
    
    
    /**
     *@param $prod_images ,$product_id.
     *@return void
     * This function used to save soldi products images data to database
     */
    public function saveProductImages($prod_images,$product_id)
    {
       
        $saveProdImages = [];
        if (count($saveProdImages) > 0) {
            foreach ($prod_images as $image) {
                $imgArr = [];
                $imgArr['product_id'] = $product_id;
                $imgArr['image_path'] = $image;
                $imgArr['created_at'] = date('Y-m-d H:i:s');
                $imgArr['updated_at'] = date('Y-m-d H:i:s');
                $saveProdImages[] = $imgArr;
            }
       
           SoldiLog::info("  ".basename(__FILE__)."  product images are added for productID:".$product_id);
           DB::table('product_images')->insert($saveProdImages);
        }else{
            SoldiLog::info("  ".basename(__FILE__)."  no product images are added for productID:".$product_id);
        }
    }        
    
     /**
     *@param $variants ,$product_id.
     *@return void
     * This function used save soldi products variants data to database
     */
    public function saveProductVariants($paramas = [])
            
    {
        
        extract($paramas);
        if(count($variants_array) > 0){
            foreach($variants_array as $variant){
                
                $this->variantId = $this->initializeVariantAutoID(count($variant));
                $product_id = $variant["product_id"];
                unset($variant["product_id"]);
                if(count($variant) > 0){
                   foreach($variant as $var){
                    
                    $data = $this->mapper->mapFields((array)$var, (array) json_decode($this->variant_mapper));
                    $this->variantData[] = array_merge($data,['variant_id'=> $this->variantId,'created_at'=> Carbon::now()]);
                    $this->variants += 1;
                    $this->productVariantData[] = ['product_id' => $product_id, "variant_id" => $this->variantId,'created_at'=> Carbon::now()];
                    SoldiLog::info("  ".basename(__FILE__)."  Variant with id: ".$this->variantId." prepared for Saving.");
                    ++$this->variantId;
                   }
                }
                
            }// end foreach
            
        }// end if
//        echo "<pre>";
//        print_r($variants_array);
//        exit;
//        die("here");
//        if(count($variants) > 0)
//        {
//            
//            $variantDataFields = json_decode($this->variant_mapper, true);
//            foreach($variants as $variant)
//            {
//                $varSaveData = BasePosMapper::mapFields($variant, $variantDataFields);
//                $newVariant = Variant::create($varSaveData);
//                SoldiLog::info("  ".basename(__FILE__)."  product variant ".$varSaveData['variant_name']."are added for productID:".$product_id);
//                
//                    if($newVariant->variant_id > 0 ):
//                    
//                    $prodVariant = [];
//                    $prodVariant['product_id'] = $product_id;
//                    $prodVariant['variant_id'] = $newVariant->variant_id;
//                    ProductVariant::create($prodVariant);
//                    SoldiLog::info("  ".basename(__FILE__)."  product: ".$product_id." and Variant : ".$newVariant->variant_id." relation created ");
//                    $this->variants++;
//                    endif;
//            }
//        }
    }     

    
     /**
     *@param $optionSets ,$product_id,$storeId.
     *@return void
     * This function used save soldi products options set data to database
     */
    public function saveProductOptionSet($params = [])
            
    {
        extract($params);
        if(count($optionset_array)){
            
            
            foreach($optionset_array as $optionset){
                
                $this->optionSetId = $this->initializeOptionSetAutoID(count($optionset));
                $product_id = $optionset["product_id"];
                $store_id   = $optionset["store_id"];
                unset($optionset["product_id"]);
                unset($optionset["store_id"]);
                
                if(count($optionset) > 0){
                    
                    foreach($optionset as $opset){
                        if(!empty($opset)){
                            $data = $this->mapper->mapFields((array)$opset, (array) json_decode($this->optionset_mapper));
                            $this->optionSetData[]     = array_merge($data,['option_set_id'=> $this->optionSetId,'store_id'=>$store_id,'pos_code'=>$this->pos_code,'created_at'=> Carbon::now()]);
                            $this->productOptionData[] = ['product_id' => $product_id, "option_set_id" => $this->optionSetId,'created_at'=> Carbon::now()];
                            $this->optionset += 1;
                            ++$this->optionSetId;
                        }
                        
                        if(count($opset->options)){
                            
                            $options_array         = $opset->options;
                            $final_option_array[]  = array_merge($options_array,["product_id"=> $product_id,"option_set_id"=>$this->optionSetId]);
                        }
                    }
                    
                }
                
            }
            $this->saveProductOptions(["option_array"=>$final_option_array]);
        }
       
//        if(count($optionSets) > 0):
//            
//            
//            $optionSetMapData = json_decode($this->optionset_mapper, true);
//            
//            foreach($optionSets as $optSet):
//                
//                $optSetSaveData = BasePosMapper::mapFields($optSet, $optionSetMapData);
//                $optSetSaveData['pos_code'] = 1;
//                $optSetSaveData['store_id'] = $this->store_ids[$storeId];
//                $newOptset = Optionset::create($optSetSaveData);
//                SoldiLog::info("  ".basename(__FILE__)."  product option set ".$optSetSaveData['option_set_name']."are added for productID:".$product_id);
//                
//                if($newOptset->option_set_id > 0 ):
//                    $this->saveProductOptions ($optSet['options'], $product_id);
//                    $prodOptSet = [];
//                    $prodOptSet['product_id'] = $product_id;
//                    $prodOptSet['option_set_id'] = $newOptset->option_set_id;
//                    ProductOption::create($prodOptSet);
//                    SoldiLog::info("  ".basename(__FILE__)."  product: ".$product_id." and option set : ".$newOptset->option_set_id." relation created ");
//                    $this->optionset++;
//                endif; 
//                
//            endforeach;
//                
//        endif;    
    }
    
    
     /**
     *@param $options ,$product_id.
     *@return void
     * This function used save soldi products options data to database
     */
    public function saveProductOptions($params = [])
            
    {
        extract($params);
        
        if(count($option_array) > 0){
            
            foreach($option_array as $option){
                
                $this->optionsId = $this->initializeOptionsAutoID(count($option));
                $product_id    = $option["product_id"];
                $option_set_id = $option["option_set_id"];
                unset($option["product_id"]);
                unset($option["option_set_id"]);
                if(count($option) > 0){
                    foreach($option as $opt){
                        $data = $this->mapper->mapFields((array)$opt, (array) json_decode($this->option_mapper));
                        $this->optionsData[] = array_merge($data,['option_set_id'=> $option_set_id,'option_id'=>$this->optionsId,'created_at'=> Carbon::now()]);
                        $this->options += 1;
                        ++$this->optionsId;
                        
                    }
                }
            }
        }
        
//        if(count($options) > 0):
//            
//            $optionDataFields = json_decode($this->option_mapper, true);
//            
//            foreach($options as $option):
//                $optionSaveData = BasePosMapper::mapFields($option, $optionDataFields);
//                $newOptset = Options::create($optionSaveData);
//                SoldiLog::info("  ".basename(__FILE__)."  product option ".$optionSaveData['option_name']."are added for productID:".$product_id);
//            endforeach;
//                
//        endif; 
    }
    
     /**
     * Get All required mappers from db.
     */
    public function getMappers()
    {

        $mappers  =   $this->mapper->getMappedData(1 , false,true);
        foreach ($mappers as $mapper):
            if($mapper->service_code    ==  config("serviceConstants.SAVE_SOLDI_STORE_DATA"))
                $this->store_mapper      =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SAVE_SOLDI_CATEGORY_DATA"))
                $this->category_mapper   =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SAVE_SOLDI_PRODUCTS_DATA"))
                $this->product_mapper    =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SOLDI_PRODUCT_OPTIONSET_DATA"))
                $this->optionset_mapper =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SOLDI_PRODUCT_OPTIONS_DATA"))
                $this->option_mapper     =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SOLDI_PRODUCT_VARIANTS_DATA"))
                $this->variant_mapper    =   $mapper->fields;
        endforeach;
    }//..... end of getMappers() ......//
    
    

    /**
     * Save All Data to Database.
     */
    private function saveData()
    {
        try{
            DB::transaction(function () {
                
                if(!empty($this->StoreData))
                    DB::table('store')->insert($this->StoreData);

                if(!empty($this->categoryData))
                    DB::table('category')->insert($this->categoryData);

                if(!empty($this->productData))
                    DB::table('product')->insert($this->productData);

                if(!empty($this->optionSetData))
                  
                    DB::table('option_sets')->insert($this->optionSetData);

                if(!empty($this->variantData))
                    DB::table('variant')->insert($this->variantData);

                if(!empty($this->productVariantData))
                    DB::table('product_variant')->insert($this->productVariantData);

                if(!empty($this->productOptionData))
                    DB::table('product_option_set')->insert($this->productOptionData);
                
                if(!empty($this->optionsData))
                    DB::table('options')->insert($this->optionsData);

            }, 5);//..... Transactions Ends here.
        }catch (\Exception $exception){
            //dd("deleted the exceptio");
            SoldiLog::warning("  ".basename(__FILE__)."  ".$exception);
        }//..... end of try-catch block .....//
    }//..... end of saveData() .....//
    
    
    public function saveOptionSetData( $params = [] )
    {
        
    }

    public function saveVariantData( $params = [] )
    {
    
    }

}//..... end of class.    
    

