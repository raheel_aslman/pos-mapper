<?php

namespace App\Http\Controllers;

use App\ApiCalls\StarrtecPosApiCall;
use App\Facade\StarrtecLog;
use App\mapper\StarrtecPosMapper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class StarrtecposController extends BasePosController
{
    private $startecApiCall;
    private $mapper;
    private $pos_code;
    private $stores = 0,$categories = 0, $products = 0, $optionset = 0, $variants = 0;
    private $location_mapper, $category_mapper, $product_mapper, $option_mapper, $variant_mapper;
    private $storeAutoId, $categoryId, $productId, $optionSetId, $variantId;
    private $locationsData = [],
            $categoryData  = [],
            $productData   = [],
            $optionSetData = [],
            $variantData   = [],
            $productVariantData= [],
            $productOptionData = [];

    /**
     * StarrtecposController constructor.
     */
    public function __construct()
    {
        $this->startecApiCall = new StarrtecPosApiCall();
        $this->mapper         = new StarrtecPosMapper();
        $this->pos_code       = config('serviceConstants.STARRTEC_POS_CODE');
        $this->getMappers();
    }//..... end of constructor() .....//

    /**
     * Get Categories List.
     */
    public function populateData()
    {
        $this->printLogsHeader();
        $this->getStore();

        StarrtecLog::info("  ".basename(__FILE__)."  Saving of All data to Unified DB is Started.");
        $this->saveData();
        $this->printLogsFooter();

        return "Stores: {$this->stores}, Categories: {$this->categories}, Products: {$this->products}, Options: {$this->optionset}, Variants: {$this->variants} Saved Successfully.";
    }//..... end of getCategoriesList() ......//

    /**
     * Get Locations/Store.
     */
    public function getStore($params = [])
    {
        $locations = $this->startecApiCall->getLocations();
        $this->saveStoreData(['locations'=> $locations]);
    }//..... end of getStore() ......//

    /**
     * Save All locations data of Starrtec POS.
     */
    public function saveStoreData( $params = [] )
    {
        extract($params);

        if($locations){
            $locations = (json_decode($locations))->Locations;
            $this->storeAutoId = $this->initializeStoreAutoIncrementID( count($locations) );
            StarrtecLog::info("  ".basename(__FILE__)."  Preparing Location Details of the Starrtec POS for Saving to Unified DB is Started.");

            foreach ($locations as $key => $location){
                $data = $this->mapper->mapFields((array)$location, (array) json_decode($this->location_mapper));
                $this->locationsData[] = array_merge($data,["store_id"=> $this->storeAutoId ,'pos_code'=> $this->pos_code,'created_at'=> Carbon::now()]);
                $locations[$key]->store_id = $this->storeAutoId;
                $locations[$key]->pos_store_id = $data['pos_store_id'];
                $this->stores += 1;
                StarrtecLog::info("  ".basename(__FILE__)."  Preparation of Location of the Starrtec POS with store_id:".$this->storeAutoId." is done.");
                ++$this->storeAutoId;
            }//..... end of foreach() ......//

            StarrtecLog::info("  ".basename(__FILE__)."  Preparation of Locations Details Data of the Starrtec POS for saving to Unified DB is Completed.");

            $this->getLocationsDetails($locations);
        }else{
            StarrtecLog::warning("  ".basename(__FILE__)."  No Location Data returned from Starrtec POS API.");
        }//..... end if() ......//
    }//..... end of saveStoreData() ......//

    /**
     * @param $locations
     * Save location/store details.
     */
    public function getLocationsDetails($locations)
    {
        StarrtecLog::info("******************************************************************************************************************");
        StarrtecLog::info("  ".basename(__FILE__)."  Getting details of single Location of the Starrtec POS through API is Started.");

        $ids = array_column($locations,'pos_store_id');
        $result = $this->startecApiCall->getMultiLocationData($ids);
        $this->countDataDetails($result);

        StarrtecLog::info("  ".basename(__FILE__)."  Preparing details of each Location's Categories for saving is Started.");

        foreach ($locations as $key => $location){
            $locations[$key]->Categories = $result[$key];
            $this->saveCategoryData(["location" => $locations[$key]]);
        }//..... end foreach() .....//

        StarrtecLog::info("  ".basename(__FILE__)."  Preparation of each Location's Categories details Finished.");
    }//...... end of getLocationDetails() ......//

    /**
     * @param array $params
     * Save Category Details data.
     */
    public function saveCategoryData($params = [])
    {
        extract($params);
        if($location->Categories){
            $categories = (json_decode($location->Categories))->Categories;
            foreach ($categories as $category) {
                $data = $this->mapper->mapFields((array)$category, (array) json_decode($this->category_mapper));
                $this->categoryData[] = array_merge($data,['pos_code'=> $this->pos_code,"store_id"=> $location->store_id, "category_id"=> $this->categoryId, 'created_at'=> Carbon::now()]);
                $this->categories += 1;

                StarrtecLog::info("  ".basename(__FILE__)."  Category with id: ".$this->categoryId." prepared for Saving.");

                if(!empty($category->Products)){
                    foreach ($category->Products as $product){
                        $this->saveProductsData(["product" => $product, "category_id" => $this->categoryId, "store_id" => $location->store_id]);
                    }//..... end of inner foreach() ......//
                }//..... end if() ......//

                ++$this->categoryId;
            }//..... end of foreach() .....//
        }else{
            StarrtecLog::warning("  ".basename(__FILE__)."  No Category details for location/store id:".$location->store_id);
        }//..... end if() .....//
    }//..... end of saveCategoryData() .....//

    /**
     * @param array $params
     * Save Product details.
     */
    public function saveProductsData($params = [])
    {
        extract($params);
        $this->products += 1;

        $data = $this->mapper->mapFields((array)$product, (array) json_decode($this->product_mapper));
        $this->productData[] = array_merge($data,["category_id"=> $category_id, 'pos_code'=> $this->pos_code,"store_id"=> $store_id, "product_id"=> $this->productId, 'created_at'=> Carbon::now()]);

        StarrtecLog::info("  ".basename(__FILE__)."  Product with id: ".$this->productId.", of Category id: ".$category_id." is prepared for Saving.");

        if( !empty($product->Options) ) {
            foreach ($product->Options as $option){
                $this->saveOptionSetData(["option" => $option, "store_id" => $store_id, "product_id"=> $this->productId]);
            }//...... end of foreach() .......//
        }//..... end if() .....//
        ++$this->productId;
    }//..... end of saveProductsData() .....//

    /**
     * Save Option set data.
     * in Starrtec options are placed in option_set table.
     */
    public function saveOptionSetData($params = [])
    {
        extract($params);

        $this->optionset += 1;
        $data = $this->mapper->mapFields((array)$option, (array) json_decode($this->option_mapper));
        $this->optionSetData[] = array_merge($data,['pos_code'=> $this->pos_code,"store_id"=> $store_id, "option_set_id"=> $this->optionSetId]);

        //..... save product_option_set
        $this->productOptionData[] = ['product_id' => $product_id, "option_set_id" => $this->optionSetId, 'created_at'=> Carbon::now()];

        StarrtecLog::info("  ".basename(__FILE__)."  Option Set with id: ".$this->optionSetId.", of Product id: ".$product_id." and Store id: ".$store_id." is prepared for Saving.");

        if( !empty($option->Variants) ){
            foreach ($option->Variants as $variant){
                $this->saveVariantData(["variant" => $variant, "product_id" => $product_id]);
            }//.... end foreach() ......//
        }//..... end if() .....//

        ++$this->optionSetId;
        return;
    }//..... end of saveOptionSetData() ......//

    /**
     * Save Variant Data.
     */
    public function saveVariantData( $params = [] )
    {
        extract($params);

        $this->variants += 1;
        $data = $this->mapper->mapFields((array)$variant, (array) json_decode($this->variant_mapper));
        $this->variantData[] = array_merge($data,['variant_id'=> $this->variantId]);

        //..... save variant.
        $this->productVariantData[] = ['product_id' => $product_id, "variant_id" => $this->variantId, 'created_at'=> Carbon::now()];

        StarrtecLog::info("  ".basename(__FILE__)."  Variant with id: ".$this->variantId.", of Product id: ".$product_id." is prepared for Saving.");
        ++$this->variantId;
        return;
    }//..... end of saveVariantData() ......//

    /**
     * Get All required mappers from db.
     */
    public function getMappers()
    {
        $mappers  =   $this->mapper->getMappedData(2 , false,true);
        foreach ($mappers as $mapper):
            if($mapper->service_code    ==  config("serviceConstants.SAVE_STARRTEC_LOCATION_DATA"))
                $this->location_mapper  =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SAVE_STARRTEC_CATEGORY_DATA"))
                $this->category_mapper  =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SAVE_STARRTEC_PRODUCT_DATA"))
                $this->product_mapper   =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SAVE_STARRTEC_OPTION_SET_DATA"))
                $this->option_mapper    =   $mapper->fields;
            if($mapper->service_code    ==  config("serviceConstants.SAVE_STARRTEC_VARIANT_DATA"))
                $this->variant_mapper   =   $mapper->fields;
        endforeach;
    }//..... end of getMappers() ......//

    /**
     * Print Logs header into logs.
     */
    private function printLogsHeader()
    {
        StarrtecLog::info("************************************************************************************************************************");
        StarrtecLog::info("************************************************************************************************************************");
        StarrtecLog::info("**********************************  Fetching Data From STARRTEC POS APIs Started  **************************************");
        StarrtecLog::info("************************************************************************************************************************");
        StarrtecLog::info("************************************************************************************************************************");
    }//..... end of printLogsHeader() .....//

    /**
     * Print Logs Footer to logs file.
     */
    private function printLogsFooter()
    {
        StarrtecLog::info("                                        ########################################                        ");
        StarrtecLog::info("  ".basename(__FILE__)."             No of Stores       Fetched : ".$this->stores."               ");
        StarrtecLog::info("  ".basename(__FILE__)."             No of Categories   Fetched : ".$this->categories."           ");
        StarrtecLog::info("  ".basename(__FILE__)."             No of Products     Fetched : ".$this->products."             ");
        StarrtecLog::info("  ".basename(__FILE__)."             No of Option Set   Fetched : ".$this->optionset."            ");
        StarrtecLog::info("  ".basename(__FILE__)."             No of Variants     Fetched : ".$this->variants."             ");
        StarrtecLog::info("                                        ########################################                        ");

        StarrtecLog::info("**************************************************************************************************************************");
        StarrtecLog::info("********************  Fetching Data From STARRTEC POS APIs and Saving to Unified DB is Finished  *************************");
        StarrtecLog::info("**************************************************************************************************************************");
    }//..... end of printLogsFooter() .....//

    /**
     * Save All Data to Database.
     */
    private function saveData()
    {
        try{
            DB::transaction(function () {

                if(!empty($this->locationsData))
                    DB::table('store')->insert($this->locationsData);

                if(!empty($this->categoryData))
                    DB::table('category')->insert($this->categoryData);

                if(!empty($this->productData))
                    DB::table('product')->insert($this->productData);

                if(!empty($this->optionSetData))
                    DB::table('option_sets')->insert($this->optionSetData);

                if(!empty($this->variantData))
                    DB::table('variant')->insert($this->variantData);

                if(!empty($this->productVariantData))
                    DB::table('product_variant')->insert($this->productVariantData);

                if(!empty($this->productOptionData))
                    DB::table('product_option_set')->insert($this->productOptionData);

            }, 5);//..... Transactions Ends here.
        }catch (\Exception $exception){
            StarrtecLog::warning("  ".basename(__FILE__)."  ".$exception);
        }//..... end of try-catch block .....//
    }//..... end of saveData() .....//

    /**
     * @param $result
     * Count Categories, Products, Options, Variants
     * in the data returned from Multi Api Request.
     */
    private function countDataDetails($result)
    {
        $categoriesCount = 0;
        $productsCount = 0;
        $optionsCount = 0;
        $variantsCount = 0;

        foreach ($result as $record):
            $categories = (json_decode($record))->Categories;
            $categoriesCount = count($categories);
            foreach ($categories as $category){
                if(!empty($category)){
                    $productsCount += count($category->Products);
                    foreach ($category->Products as $product){
                        if(!empty($product->Options)){
                            $optionsCount += count($product->Options);
                            foreach ($product->Options as $option){
                                if(!empty($option->Variants)){
                                    $variantsCount += count($option->Variants);
                                }//..... end if() .....//
                            }//..... end foreach() .....//
                        }//..... end if() .....//
                    }//..... end foreach() ......//
                }//.... end if() ......//
            }//..... end of inner foreach() ......//
        endforeach;

        $this->categoryId   =   $this->initializeCategoryAutoIncrementID( $categoriesCount );
        $this->productId    =   $this->initializeProductAutoIncrementID( $productsCount );
        $this->optionSetId  =   $this->initializeOptionSetAutoID( $optionsCount );
        $this->variantId    =   $this->initializeVariantAutoID( $variantsCount );
    }//..... end of saveData() .....//

    /**
     * @param $locationID
     * Get Single Location/Store Details
     */
    public function getSingleLocationDetails($locationID)
    {
      $location = $this->startecApiCall->getSingleLocation($locationID);
      dd($location);
    }//..... end of getSingleLocationDetails() .......//

}//..... end of class.
