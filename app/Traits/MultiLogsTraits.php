<?php

namespace App\Traits;
/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/21/2017
 * Time: 10:55 AM
 */
trait MultiLogsTraits{

    /**
     * Register all log handlers.
     */
    private function registerCustomLoggers()
    {
        $this->bindStarrtecLog();
        $this->bindSoldiLog();
        $this->bindSwiftLog();
        $this->bindKountaLog();
        $this->bindInfogenesisLog();
    }//...... end of registerLoggers() ......//

    /**
     * Bind Starrtec logs to service container.
     */
    private function bindStarrtecLog()
    {
        $this->doCustomBinding("StarrtecLogger","starrtec","starrtec_pos.log");
    }//...... end of bindStarrtecLog() .....//

    /**
     * Bind Soldi Logs to service container
     */
    private function bindSoldiLog()
    {
        $this->doCustomBinding("SoldiLogger","soldi","soldi_pos.log");
    }//..... end of bindSoldiLog() ......//

    /**
     * Bind Swift Logs to Service Container.
     */
    private function bindSwiftLog()
    {
        $this->doCustomBinding("SwiftLogger","swift","swift_pos.log");
    }//..... end of bindSwiftLog() ......//

    /**
     * Bind Kounta Logs to Service Container.
     */
    private function bindKountaLog()
    {
        $this->doCustomBinding("KountaLogger","kounta","kounta_pos.log");
    }//..... end of bindKountaLog() .....//

    private function bindInfogenesisLog()
    {
        $this->doCustomBinding("InfogenesisLogger","infogenesis","infogenesis_pos.log");
    }//..... end of bindInfogenesisLog() ......//

    /**
     * @param $objectName
     * @param $logName
     * @param $fileName
     * Perform creation of custom object binding to service container.
     */
    private function doCustomBinding($objectName, $logName, $fileName)
    {
        $this->app->singleton($objectName,function ()use($logName,$fileName){
            $dateFormat = "d-M-y H:i:s";
            $output =  "%datetime% %level_name% %message%".PHP_EOL;
            $log = new \Monolog\Logger($logName);
            $stream = new \Monolog\Handler\StreamHandler(storage_path('logs/'.$fileName), \Monolog\Logger::INFO);
            //$stream = new \Monolog\Handler\RotatingFileHandler(storage_path('logs/'.$fileName), 0,\Monolog\Logger::INFO);
            $stream->setFormatter(new \Monolog\Formatter\LineFormatter($output, $dateFormat,true));
            $log->pushHandler($stream);
            return $log;
        });
    }//..... end of doCustomBinding() ......//

}//..... end of traits ......//