<?php
namespace App\Traits;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;

/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/14/2017
 * Time: 11:54 AM
 */

trait guzzleApiCalls{

    /**
     * Get API Call through Guzzle HTTP Library.
     */
    public function get($url, $header = [])
    {
        $res = (new Client())->request('GET', $url,['headers'=> $header]);
        return $res->getBody()->getContents();
    }//.... end of get() .....//

    /**
     * @param array $requestData
     * @return array
     * Multi Api Call Handler.
     */
    public function getMulti($requestData = [])
    {
        $requests = [];
        $client = new Client();

        foreach ($requestData as $reqData):
            $requests[] = new Request('GET',$reqData['url'],$reqData['header']);
        endforeach;

        $responses = Pool::batch($client, $requests, ['concurrency' => 60]);
        $response = [];
        foreach ($responses as $res) {
            $response[] =  $res->getBody()->getContents();
        }

        return $response;
    }//..... end of getMulti() .....//

    /**
     * @param $url
     * @param $header
     * @param $body
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function post($url, $header, $body)
    {
        $response = (new Client())->request('POST', $url, [
            'headers'=> $header,
            'form_params' => $body
        ]);

        return $response;
    }//..... end of post() .....//
}