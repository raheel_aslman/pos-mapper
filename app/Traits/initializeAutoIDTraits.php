<?php

namespace App\Traits;
use App\Facade\InfogenesisLog;
use App\Facade\KountaLog;
use App\Facade\SoldiLog;
use App\Facade\StarrtecLog;
use App\Facade\SwiftLog;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 7/5/2017
 * Time: 9:52 AM
 */
trait initializeAutoIDTraits{
    /**
     * @param $count
     * @return int
     * Initialize store_id.
     */
    protected function initializeStoreAutoIncrementID($count)
    {
        return $this->initializeAutoID($count, "store");
    }//..... end of initializeStoreAutoIncrementID() ......//

    /**
     * @param $count
     * @return int
     * Initialize CategoryID.
     */
    protected function initializeCategoryAutoIncrementID($count)
    {
        return $this->initializeAutoID($count, 'category');
    }//..... end of initializeCategoryAutoIncrementID() .....//

    /**
     * @param $count
     * @return int
     * Initialize productID.
     */
    protected function initializeProductAutoIncrementID($count)
    {
        return $this->initializeAutoID($count, 'product');
        //return $this->initializeAutoID($count, 'product');
    }//..... end of initializeProductAutoIncrementID() .....//

    /**
     * @param $count
     * @return int
     * Initialize optionSetId.
     */
    protected function initializeOptionSetAutoID($count)
    {
        return $this->initializeAutoID($count, 'option_sets');
    }//..... end of initializeOptionSetAutoID() .....//

    /**
     * @param $count
     * @return int
     * Initialize VariantAutoID.
     */
    protected function initializeVariantAutoID($count)
    {
        return $this->initializeAutoID($count, 'variant');
    }//..... end of initializeVariantAutoID() .....//

    /**
     * @param $count
     * @return int
     * Initialize OptionsAutoID.
     */
    protected function initializeOptionsAutoID($count)
    {
        return $this->initializeAutoID($count, 'options');
    }//..... end of initializeVariantAutoID() .....//

    /**
     * @param $count
     * @param $table
     * @return int
     * Initialize Auto ID of specified table.
     */
    private function initializeAutoID($count, $table)
    {
        $AutoId = 0;
        try {
            DB::transaction(function () use($count, &$AutoId,$table) {
                $statement = DB::select("show table status like '".$table."'");
                $AutoId = $statement[0]->Auto_increment;
                $nextId = $AutoId + $count;
                DB::statement("ALTER TABLE $table AUTO_INCREMENT = ".$nextId.";");
            }, 5);

            return $AutoId;
        } catch (\Exception $e) {
            $this->printException($e);
        }//..... end of try-catch block() ......//
    }//..... end of initializeAutoID() ......//

    /**
     * @param $e
     * Print Exception for initialization of Auto ID.
     */
    private function printException($e)
    {
        if( $this instanceof \App\Http\Controllers\StarrtecposController)
            StarrtecLog::warning("  ".basename(__FILE__)."  ".$e);

        if( $this instanceof \App\Http\Controllers\SoldiPosController)
            SoldiLog::warning("  ".basename(__FILE__)."  ".$e);

        if( $this instanceof \App\Http\Controllers\KountaPosController)
            KountaLog::warning("  ".basename(__FILE__)."  ".$e);

        if( $this instanceof \App\Http\Controllers\InfogenesisPosController)
            InfogenesisLog::warning("  ".basename(__FILE__)."  ".$e);

        if( $this instanceof \App\Http\Controllers\SwiftPosController)
            SwiftLog::warning("  ".basename(__FILE__)."  ".$e);
    }//..... end of printException() ......//

}//..... end of trait{} .....//
