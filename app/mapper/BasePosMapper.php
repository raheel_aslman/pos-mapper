<?php
/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/14/2017
 * Time: 3:23 PM
 */

namespace App\mapper;

use App\interfaces\PosMapper as PosMapperInterface;
use App\Models\PosMapper;

class BasePosMapper implements PosMapperInterface
{
    
    /**
     * @param $service_code
     * @param $fieldsOnly: if true only 'fields' value will be returned.
     * @param $posID: means we have to use service_code as pos_id.
     * @return mixed
     * Get Specific Service/mapper data for mapping.
     */
    
    public static function getMappedData($service_code, $fieldsOnly = false, $posID = true)
    {

        if($posID){
            return PosMapper::wherePosId($service_code)->get(['fields','service_code']);
        } elseif($fieldsOnly)
            return (PosMapper::select('fields')->whereServiceCode($service_code)->first())->fields;
        else
            return PosMapper::whereServiceCode($service_code)->first();
    }//..... end of getMappedData() ......//
    
    
    /**
     * @param $receivedData, Data to be mapped.
     * @param $fields, fields that are going to map data against.
     * @return mixed
     *
     */
    public static function mapFields($receivedData, $fields)
    {
        //dd($receivedData);
        $saveData = [];
        foreach($fields as $pos_field => $api_field)
        {
            $field_value = '';
            if(is_array($api_field))
            {
                foreach($api_field as $f)
                {
                    $field_value .= (isset($receivedData[$f])) ? $receivedData[$f]." " : "";
                }
            } else {
                $field_value = (isset($receivedData[$api_field])) ? $receivedData[$api_field] : "";
            }

            
                $saveData[$pos_field] = $field_value;
        }
        return $saveData;
    }//..... end of mapFields() ......//
}
