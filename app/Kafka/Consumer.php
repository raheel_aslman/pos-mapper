<?php
/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/23/2017
 * Time: 2:31 PM
 */

namespace App\Kafka;


class Consumer
{

    public function __construct()
    {
        $this->initConsumer();
    }

    public function initConsumer()
    {
        date_default_timezone_set('PRC');
        $dateFormat = "d-M-y H:i:s";
        $output =  "%datetime% %level_name% %message%".PHP_EOL;
        $logger = new \Monolog\Logger('kafka');
        $stream = new \Monolog\Handler\StreamHandler(storage_path('logs/kafka.log'), \Monolog\Logger::INFO);
        $stream->setFormatter(new \Monolog\Formatter\LineFormatter($output, $dateFormat,true));
        $logger->pushHandler($stream);

        $config = \Kafka\ConsumerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList('192.168.1.6:9092');
        $config->setGroupId('maxpos');
        //$config->setBrokerVersion('0.10.2.1');
        $config->setTopics(array('maxpos'));
//$config->setOffsetReset('earliest');
        $consumer = new \Kafka\Consumer();
        $consumer->setLogger($logger);
        $consumer->start(function($topic, $part, $message) {
            echo "<pre>";
            print_r($message);
        });
    }
}