<?php

namespace App\Kafka;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 6/23/2017
 * Time: 12:19 PM
 */
class Producer
{
    public function __construct()
    {
        $this->initProducer();
    }

    public function initProducer()
    {
        date_default_timezone_set('PRC');

        $logger = new Logger('my_logger');

        $logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

        $logger->info("Sending Data to Kafka Server Started.");
        $config = \Kafka\ProducerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList('192.168.1.6:9092');
        $config->setBrokerVersion('0.10.2.1');
        $config->setRequiredAck(1);
        $config->setIsAsyn(false);
        $config->setProduceInterval(500);
        $producer = new \Kafka\Producer(function() {
            return array(
                array(
                    'topic' => 'maxpos',
                    'value' => 'this is message from maxpos.',
                    'key'   => 'testkey',
                ),
            );
        });
        $producer->setLogger($logger);
        $producer->success(function($result) {
            echo "<pre>";
            print_r($result);
        });
        $producer->error(function($errorCode, $context) {
            echo "<pre>";
            print_r($errorCode);
        });
        $producer->send();
    }
}