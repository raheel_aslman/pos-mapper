<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = 'order_detail';
    protected $primaryKey = 'order_details_id';
    protected $guarded = ['order_details_id'];
}
