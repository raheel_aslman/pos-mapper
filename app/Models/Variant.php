<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $table        = 'variant';
    protected $primaryKey   = 'variant_id';
    protected $guarded      = ['variant_id'];
}
