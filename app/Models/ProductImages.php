<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table        = 'product_images';
    protected $primaryKey   = 'product_image_id';
    protected $guarded      = ['product_image_id'];
}
