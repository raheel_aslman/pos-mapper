<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $table      = 'product_variant';
    protected $primaryKey = 'pv_id';
    protected $guarded    = ['pv_id'];
}
