<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Optionset extends Model
{
    protected $primaryKey   = 'option_set_id';
    protected $table        = 'option_sets';
    protected $guarded      = ['option_set_id'];
}
