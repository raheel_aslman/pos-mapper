<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PosMapper extends Model
{
    protected $table = "pos_mapper";
    protected $guarded = ['id'];
}
