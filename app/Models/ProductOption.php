<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table        =   'product_option_set';
    protected $primaryKey   =   'product_option_id';
    protected $guarded      =   ['product_option_id'];
}
