<?php

return [

    // 1-100 for soldi
    // 101-200 for startec
    
    
    "SOLDI_POS_CODE"               =>1,
    "SAVE_SOLDI_STORE_DATA"        =>1,
    "SAVE_SOLDI_CATEGORY_DATA"     =>2,
    "SAVE_SOLDI_PRODUCTS_DATA"     =>3,
    "SOLDI_PRODUCT_VARIANTS_DATA"  =>4,
    "SOLDI_PRODUCT_OPTIONSET_DATA" =>5,
    "SOLDI_PRODUCT_OPTIONS_DATA"   =>6,

    //.... Starrtec POS configurations.
    "STARRTEC_POS_CODE"             => 2,
    "SAVE_STARRTEC_LOCATION_DATA"   => 101, //.... location => store.
    "SAVE_STARRTEC_CATEGORY_DATA"   => 102,
    "SAVE_STARRTEC_PRODUCT_DATA"    => 103,
    "SAVE_STARRTEC_OPTION_SET_DATA" => 104,
    "SAVE_STARRTEC_VARIANT_DATA"    => 105,
];

