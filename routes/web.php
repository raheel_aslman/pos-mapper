<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/getSoldiStores', 'SoldiPosController@populateData');


Route::get("pos-data",function (\Illuminate\Http\Request $request){


    if($request->id == 'soldi'){
        try{
            return (new \App\Http\Controllers\SoldiPosController())->populateData();
        }catch (\Exception $exception){
            \App\Facade\SoldiLog::warning("  ".basename(__FILE__)."  ".$exception);
        }//..... end of try-catch block.
    } elseif($request->id == 'starrtec'){
        try{
            return (new \App\Http\Controllers\StarrtecposController())->populateData();
        }catch (\Exception $exception){
            \App\Facade\StarrtecLog::warning("  ".basename(__FILE__)."  ".$exception);
        }//..... end of try-catch block.
    }else{
        return "Your Action doesn't recognised.";
    }//..... end of if-else block() ......//

})->name("read-pos-data");



Route::get("location/{id}",function ($id){
    try{
        return (new \App\Http\Controllers\StarrtecposController())->getSingleLocationDetails($id);
    }catch (\Exception $exception){
        \App\Facade\StarrtecLog::warning("  ".basename(__FILE__)."  ".$exception);
    }//..... end of try-catch block.
});

Route::get("place-order",function(){
    return (new \App\Http\Controllers\OrderController())->placeOrder();
});


Route::get('kafka-producer',function(){
     new App\Kafka\Producer();
});

Route::get('kafka-consumer',function(){
    new App\Kafka\Consumer();
});
