<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>POS Integrations</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Styles -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <style>


            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="m-b-md" id="message">
                    Click on the following button to fetch data from respective POS.
                </div>

                <div class="links">
                    <button class="btn btn-primary pos" id="starrtec">Starrtec POS</button>
                    <button class="btn btn-primary pos" id="soldi">Soldi POS</button>
                </div>
            </div>
        </div>
        <script>
            $("document").ready(function(){
                $("button").prop("disabled",false);

                $('.pos').on('click',function (e) {
                    var id = $(this).attr('id');
                    $("button").prop("disabled",true);
                    $("#message").html("");
                    $("#message").html("<img src='{{ asset("images/ajax-loader.gif") }}' style='width:200px;'>");

                    $.ajax({
                        url:'{{ route('read-pos-data') }}',
                        type:'get',
                        data:{id:id},
                        success:function (res) {
                            $("#message").html("");
                            $("#message").html(res);
                            $("button").prop("disabled",false);
                        },error:function (err) {
                            $("#message").html("");
                            $("#message").html("Internal Server Error Occurred.");
                            $("button").prop("disabled",false);
                        }
                    });
                });
            });
        </script>
    </body>
</html>
