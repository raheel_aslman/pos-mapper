<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use GuzzleHttp\Client;
class soldiPosTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
   public function testSoldiStoreResponse()
    {
        $url = "https://dev.mysoldi.co/CST/api/v1/app/business/detail?business_id=500";
        $header =  ['X-API-KEY' => "OTM1ODVlMjdkYTc2MzFhMTE2OTQ2Zj",'SECRET' => "NzZmNmEzMjA3ZjViOWE0MzM2ZjE2Zj"];
       $client = new Client();
        $res = $client->request('GET', $url, [
                'headers' => $header,
            ]);
        
        if($res->getStatusCode() == 200)
            $this->assertTrue (true);
        else
            $this->assertTrue (false);
        
         
    }
    
    public function testSoldiStoreData()
    {
        $url = "https://dev.mysoldi.co/CST/api/v1/app/business/detail?business_id=500";
        $header =  ['X-API-KEY' => "OTM1ODVlMjdkYTc2MzFhMTE2OTQ2Zj",'SECRET' => "NzZmNmEzMjA3ZjViOWE0MzM2ZjE2Zj"];
       $client = new Client();
        $res = $client->request('GET', $url, [
                'headers' => $header,
            ]);
       $content =  $res->getBody()->getContents();
       $responseArr = json_decode($content, true);
            $storeInfo = $responseArr['data']['Business_Information'];
        if(count($storeInfo) > 0)
            $this->assertTrue (true);
        else
            $this->assertTrue (false);
        
    }
    
    public function testSoldiCategoryResponse()
    {
        $url = "https://www.dev.mysoldi.co/CST/api/v1/app/pos/category?business_id=500";
        $header =  ['X-API-KEY' => "OTM1ODVlMjdkYTc2MzFhMTE2OTQ2Zj",'SECRET' => "NzZmNmEzMjA3ZjViOWE0MzM2ZjE2Zj"];
       $client = new Client();
        $res = $client->request('GET', $url, [
                'headers' => $header,
            ]);
        
        if($res->getStatusCode() == 200)
            $this->assertTrue (true);
        else
            $this->assertTrue (false);
        
         
    }
    
    public function testSoldiCategoryData()
    {
        $url = "https://www.dev.mysoldi.co/CST/api/v1/app/pos/category?business_id=500";
        $header =  ['X-API-KEY' => "OTM1ODVlMjdkYTc2MzFhMTE2OTQ2Zj",'SECRET' => "NzZmNmEzMjA3ZjViOWE0MzM2ZjE2Zj"];
       $client = new Client();
        $res = $client->request('GET', $url, [
                'headers' => $header,
            ]);
        $content =  $res->getBody()->getContents();
       $responseArr = json_decode($content, true);
        $categoriesInfo = $responseArr['data'];
        if(count($categoriesInfo) > 0)
            $this->assertTrue (true);
        else
            $this->assertTrue (false);
        
         
    }
    
    public function testSoldiProductResponse()
    {
        $url = "https://dev.mysoldi.co/CST/api/v1/app/inventory/listproducts?business_id=500&cate_id=1353";
        $header =  ['X-API-KEY' => "OTM1ODVlMjdkYTc2MzFhMTE2OTQ2Zj",'SECRET' => "NzZmNmEzMjA3ZjViOWE0MzM2ZjE2Zj"];
       $client = new Client();
        $res = $client->request('GET', $url, [
                'headers' => $header,
            ]);
        
        if($res->getStatusCode() == 200)
            $this->assertTrue (true);
        else
            $this->assertTrue (false);
        
         
    }
    
    public function testSoldiProductData()
    {
        $url = "https://dev.mysoldi.co/CST/api/v1/app/inventory/listproducts?business_id=500&cate_id=1353";
        $header =  ['X-API-KEY' => "OTM1ODVlMjdkYTc2MzFhMTE2OTQ2Zj",'SECRET' => "NzZmNmEzMjA3ZjViOWE0MzM2ZjE2Zj"];
       $client = new Client();
        $res = $client->request('GET', $url, [
                'headers' => $header,
            ]);
        $content =  $res->getBody()->getContents();
       $responseArr = json_decode($content, true);
        $productData = $responseArr['data'];
        if(count($productData) > 0)
            $this->assertTrue (true);
        else
            $this->assertTrue (false);
        
         
    }
}
