<?php

namespace Tests\Unit;
use App\ApiCalls\SoldiPosApiCall;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SoldiPosApiCallTest extends TestCase
{
    
    private $all_stores,$store_data,$soldipac,$apiKey,$secretKey,$storeId,$store_categories,$products_data;
    
    public function __construct()
    {
        $this->createApplication();
        $this->soldipac    = new SoldiPosApiCall();
        
    }

    /**
     * check All Stores.
     *
     * @return void
     */
    public function testCheckAllStores()
    {
        $this->initializeAllStores();
        $this->allStoresShouldBeReturned();
        $storesArr = json_decode($this->all_stores,true);
        
        if(count($storesArr["data"]) > 0){
            
            foreach($storesArr["data"] as $stores){
                
                $this->initializeStoreData($stores["business_api_key"], $stores["business_secret"], $stores["business_id"]);
                $this->storeDataShouldBeReturned();
            }
        }
    }   
    
    
            /**
     * Initialize Store data.
     *
     * @return void
     */
    
    private function initializeAllStores()
    {
        $this->all_stores = $this->soldipac->getAllStores();
         
    }
    
    /**
     * The store data should be return.
     *
     * @return void
     */
    
    private function allStoresShouldBeReturned()
    {
        
        if(count($this->all_stores) > 0)
            $this->assertTrue(true);
        else
            $this->assertTrue(false);
    }

    
    /**
     * Initialize Store data.
     *
     * @return void
     */
    
    private function initializeStoreData($apiKey, $secretKey, $storeId)
    {
        $this->store_data = $this->soldipac->getStore($apiKey, $secretKey, $storeId);
         
    }
    
    /**
     * The store data should be return.
     *
     * @return void
     */
    
    private function storeDataShouldBeReturned()
    {
        
        if(count($this->store_data) > 0)
            $this->assertTrue(true);
        else
            $this->assertTrue(false);
    }
    
    
    /**
     * check Store categories.
     *
     * @return void
     */
    public function testCheckStoreCategories()
    {
        
        $this->initializeAllStores();
        $storesArr = json_decode($this->all_stores,true);
        if(count($storesArr["data"]) > 0){
            
            foreach($storesArr["data"] as $stores){
                
                $this->initilizeStoreCategories($stores["business_api_key"], $stores["business_secret"], $stores["business_id"]);
                $this->storeCategoriesShouldBeReturned();
            }
        }
    }

    private function initilizeStoreCategories($apiKey, $secretKey, $storeId)
    {
       $this->store_categories = $this->soldipac->getCategories($apiKey, $secretKey, $storeId);
       
    }

    
    private function storeCategoriesShouldBeReturned()
    {
       
        if(count($this->store_categories) > 0)
            $this->assertTrue(true);
        else
            $this->assertTrue(false);
    }
    
    public function testCheckCategoriesProducts()
    {
        
        $this->initializeAllStores();
        $storesArr = json_decode($this->all_stores,true);
        if(count($storesArr["data"]) > 0){
            foreach($storesArr["data"] as $stores){
                
                $this->initilizeStoreCategories($stores["business_api_key"], $stores["business_secret"], $stores["business_id"]);
                $data   = (json_decode($this->store_categories))->data;
                $catids = [];
                array_walk_recursive($data,function ($category)use(&$catids){
                    $obj = new \stdClass();
                    $obj->pos_category_id = $category->cate_id;
                    $catids[] = $obj ;
                });
                $this->initilizeStoreCategoriesProducts($stores["business_api_key"], $stores["business_secret"], $stores["business_id"],$catids);
                $this->storeCategoriesProductsShouldBeReturned();
            }
        }
    }
    
    private function initilizeStoreCategoriesProducts($apiKey, $secretKey, $storeId,$catids)
    {
        $this->products_data = $this->soldipac->getProducts($apiKey, $secretKey, $storeId,$catids);
    }
    
    private function storeCategoriesProductsShouldBeReturned()
    {
         if($this->products_data)
             $this->assertTrue(true);
         else
             $this->assertTrue(false);
    }    
    
}// end class SoldiPosApiCallTest
 
    
