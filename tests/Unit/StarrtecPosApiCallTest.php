<?php

namespace Tests\Unit;

use App\ApiCalls\StarrtecPosApiCall;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StarrtecPosApiCallTest extends TestCase
{
    private $location_data,$spac;
    private $multi_location_data,$single_location_data;

    /**
     * StarrtecPosApiCallTest constructor.
     */
    public function __construct()
    {
        $this->createApplication();
        $this->spac = new StarrtecPosApiCall();
    }//..... end of __construct() ......//

    /**
     * Check Locations data.
     *
     * @return void
     */
    public function testCheckLocationData(): void
    {
        $this->initializeLocationData();
        $this->locationsShouldBeReturned();
    }//..... end of testCheckLocationData() .....//

    /**
     * Initialize location data.
     */
    private function initializeLocationData(): void
    {
        $this->location_data = $this->spac->getLocations();
    }//..... end of initializeLocationData() .....//

    /**
     * Check Location data.
     */
    private function locationsShouldBeReturned(): void
    {
        $this->assertNotEmpty($this->location_data);
    }//..... end of locationsShouldBeReturned() .....//

    /**
     * Check Single location data.
     */
    public function testSingleLocationData(): void
    {
        $this->initializeLocationData();
        $locations = (json_decode($this->location_data))->Locations;

        foreach ($locations as $location):
            $this->initializeSingleLocationData($location->Id);
            $this->singleLocationDataShouldBeReturned();
        endforeach;
    }//..... end of testSingleLocationData() .....//

    /**
     * @param $location_id
     * Initialize Single Location Data.
     */
    private function initializeSingleLocationData($location_id): void
    {
        $this->single_location_data = $this->spac->getLocationData($location_id);
    }//..... end of initializeSingleLocationData() ......//

    /**
     * Check single location data.
     */
    private function singleLocationDataShouldBeReturned(): void
    {
        $this->assertNotEmpty($this->single_location_data);
    }//..... end of singleLocationDataShouldBeReturned() .....//

    /**
     * Check Multi Location function.
     */
    public function testMultiLocationData(): void
    {
        $this->initializeLocationData();
        $data = (json_decode($this->location_data))->Locations;
        $ids = [];
        array_walk_recursive($data,function ($location)use(&$ids){
            $ids[] = $location->Id;
        });

        $this->multiLocationDataInitialized($ids);
        $this->multiLocationsDataShouldBeReturned();
    }//..... end of testMultiLocationData() .....//

    /**
     * @param $ids
     * Initialize Multi Location data.
     */
    private function multiLocationDataInitialized($ids): void
    {
        $this->multi_location_data = $this->spac->getMultiLocationData($ids);
    }//..... end of multiLocationDataInitialized() .....//

    /**
     * Check Multi Location Data.
     */
    private function multiLocationsDataShouldBeReturned(): void
    {
         $this->assertNotEmpty($this->multi_location_data);
    }//..... end of multiLocationsDataShouldBeReturned() .....//
}
