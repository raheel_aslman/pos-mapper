<?php

namespace Tests\Unit;

use App\mapper\BasePosMapper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BasePosMapperTest extends TestCase
{
    private $mapper_data;

    /**
     * BasePosMapperTest constructor.
     */
    public function __construct()
    {
        $this->createApplication();
    }//..... end of __construct() ......//

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetMappedData(): void
    {
        $this->initializeMapperData(101);
        $this->mapperDataShouldBeReturned();
    }//..... end of testGetMappedData() .....//

    /**
     * @param $service_code
     * Initialize Mapper Data.
     */
    private function initializeMapperData($service_code): void
    {
        $this->mapper_data = BasePosMapper::getMappedData($service_code);
    }//..... end of initializeMapperData() .....//

    /**
     * Check data returned from mapper function.
     */
    private function mapperDataShouldBeReturned(): void
    {
        $this->assertNotEmpty($this->mapper_data,'The data should be returned from getMappedData function.');
    }//..... end of mapperDataShouldBeReturned() ......//

    /**
     * Test GetMappedData Function with invalid service_code.
     */
    public function testGetMappedDataWithInvalidValue(): void
    {
        $this->initializeMapperData(5000);
        $this->getMappedDataShouldNotReturnData();
    }//..... end of testGetMappedDataWithInvalidValue() ......//

    /**
     * assert mapped data with invalid service code.
     */
    private function getMappedDataShouldNotReturnData(): void
    {
        $this->assertEmpty($this->mapper_data,'The data should not be returned.');
    }//..... end of getMappedDataShouldNotReturnData() ......//

    /**
     * Test mapFields function.
     */
    public function testMapFields(): void
    {
        $this->initializeLocationMapperData();
        $this->mapFieldsShouldReturnExpectedData($this->getFakeLocationDataArray());
    }//...... end of testMapFields() ......//

    /**
     * @return array
     * Generate Fake data array.
     */
    private function getFakeLocationDataArray(): array
    {
        return  [
            "Id"        => "2495",
            "Name"      => "Club Starrtec",
            "Phone"     => "+61 7 3808 9917",
            "Email"     => "bookings@starrtec.com.au",
            "WebSite"   => "http://www.starrtec.com.au",
            "Address"   => "3/1 Balmain St Underwood"
        ];
    }//..... end of testMapFields() .....//

    /**
     * initialize mapper data.
     */
    private function initializeLocationMapperData(): void
    {
        $this->initializeMapperData(101);
        $this->mapper_data = (array) json_decode($this->mapper_data->fields);
    }//..... end of initializeLocationMapperData() ......//

    /**
     * @param $data
     * Perform Mapping and assertions.
     */
    private function mapFieldsShouldReturnExpectedData($data): void
    {
        $map_fields = BasePosMapper::mapFields($data, $this->mapper_data);

        $this->assertArrayHasKey('pos_store_id', $map_fields);
        $this->assertArrayHasKey('store_name',   $map_fields);
        $this->assertArrayHasKey('store_phone',  $map_fields);
        $this->assertArrayHasKey('store_email',  $map_fields);
        $this->assertArrayHasKey('store_website',$map_fields);
        $this->assertArrayHasKey('store_address',$map_fields);
    }//..... end of mapFieldsShouldReturnExpectedData() ......//
}//..... end of class.
