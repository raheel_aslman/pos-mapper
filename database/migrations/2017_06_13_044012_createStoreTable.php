<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store', function (Blueprint $table) {
            $table->increments('store_id');
            $table->integer("pos_id")->nullalble();
            $table->integer('venus_id')->default(0);
            $table->string("store_name")->nullable();
            $table->string("store_phone")->nullable();
            $table->string('store_email')->nullable();
            $table->string("store_website")->nullable();
            $table->string("store_contact")->nullable();
            $table->text("store_address")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store');
    }
}
