<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category', function (Blueprint $table) {
            $table->bigInteger('store_id')->after('pos_category_id');
            $table->string("category_type",20)->nullable();
            $table->string("category_color")->nullable();
            $table->string("display_priority")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category', function (Blueprint $table) {
            $table->dropColumn('store_id');
            $table->dropColumn('category_type');
            $table->dropColumn('category_color');
            $table->dropColumn('display_priority');
        });
    }
}
