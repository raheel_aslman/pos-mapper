<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifiyStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store', function (Blueprint $table) {
            $table->string("store_owner")->nullable();
            $table->string("store_account_type")->nullable();
            $table->text("store_detail")->nullable();
            $table->text("store_detail_info")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store', function (Blueprint $table) {
            $table->dropColumn('store_owner');
            $table->dropColumn('store_account_type');
            $table->dropColumn('store_detail');
            $table->dropColumn('store_detail_info');
        });
    }
}
